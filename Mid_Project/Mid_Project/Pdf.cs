﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Paragraph = iTextSharp.text.Paragraph;
using SaveFileDialog = System.Windows.Forms.SaveFileDialog;

namespace Mid_Project
{
    public  class Pdf
    {
        
        public static Document CustomizeAdvisoryReport()
        {
            var document = new Document();

            try
            {
                // Create a SaveFileDialog to prompt the user for the file path
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF files (*.pdf)|*.pdf";
                saveFileDialog.Title = "Save PDF Report";
                saveFileDialog.FileName = "Project_Advisory_Report.pdf"; // Default file name

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Initialize a PdfWriter with the chosen file path
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(saveFileDialog.FileName, FileMode.Create));
                    document.Open();

                    // Add a title to the report
                    var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);
                    var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 10, BaseColor.BLACK); // smaller font size and normal weight
                    Paragraph reportTitle = new Paragraph("Project Details with Advisory Board", boldFont);
                    reportTitle.SpacingBefore = 20f;
                    reportTitle.SpacingAfter = 20f;
                    reportTitle.Font.Size = 24;
                    reportTitle.Alignment = Element.ALIGN_CENTER;
                    document.Add(reportTitle);

                    // Retrieve project and advisory board information
                    DataTable projectData = GetProjectAndAdvisoryBoardData();

                    // Create a table to display the data
                    PdfPTable table = new PdfPTable(projectData.Columns.Count);
                    table.WidthPercentage = 100;

                    // Add column headers to the table
                    foreach (DataColumn column in projectData.Columns)
                    {
                        PdfPCell headerCell = new PdfPCell(new Phrase(column.ColumnName, normalFont)); // smaller font size and normal weight
                        headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        headerCell.BackgroundColor = new BaseColor(128, 128, 128); // Gray background color
                        table.AddCell(headerCell);
                    }

                    // Add rows to the table
                    foreach (DataRow row in projectData.Rows)
                    {
                        foreach (object item in row.ItemArray)
                        {
                            string cellValue = (item == DBNull.Value) ? "null" : item.ToString(); // Handle DBNull values
                            PdfPCell dataCell = new PdfPCell(new Phrase(cellValue, normalFont)); // smaller font size and normal weight
                            dataCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(dataCell);
                        }
                    }

                    // Add the table to the document
                    document.Add(table);

                    // Close the document
                    document.Close();
                    writer.Close();
                }
                else
                {
                    document = null; // Return null if the user cancels the operation
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return document;
        }


        public static DataTable GetProjectAndAdvisoryBoardData()
        {
            DataTable dataTable = new DataTable();

            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT MAX(P.Title) AS Title, MAX(P.Description) AS [Project Description], " +
                                                "MAX(CASE WHEN L.Value='Main Advisor' THEN CONCAT(Person.FirstName,' ',Person.LastName) END) AS [Main Advisor], " +
                                                "MAX(CASE WHEN L.Value='Co-Advisor' THEN CONCAT(Person.FirstName,' ',Person.LastName) END) AS [Co Advisor], " +
                                                "MAX(CASE WHEN L.Value='Industry Advisor' THEN CONCAT(Person.FirstName,' ',Person.LastName) END) AS [Industry Advisor] " +
                                                "FROM ProjectAdvisor PA " +
                                                "INNER JOIN Advisor A ON PA.AdvisorId = A.Id " +
                                                "JOIN Project P ON P.Id=PA.ProjectId " +
                                                "JOIN Person ON Person.Id=A.Id " +
                                                "JOIN Lookup L ON L.Id=PA.AdvisorRole " +
                                                "GROUP BY PA.ProjectId", con);

                SqlDataReader reader = cmd.ExecuteReader();
                dataTable.Load(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dataTable;
        }


        public static void MarkSheet()
        {
            try
            {
                List<int> evaluationIds = GetEvaluationIds();
                List<string> evaluationTitle = GetEvaluationTitle();
                if (evaluationIds.Count > 0)
                {
                    // Prepare the query to fetch data for each evaluation
                    string query = "";
                    int idx = 0;
                    foreach (int evaluationId in evaluationIds)
                    {
                        query += ",MAX(CASE WHEN E.Id=" + evaluationId + " THEN GE.ObtainedMarks END) AS [" + evaluationTitle[idx] + "]";
                        idx++;
                    }

                    // Ask the user to choose the location to save the PDF
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "PDF files (*.pdf)|*.pdf";
                    saveFileDialog.Title = "Save Mark Sheet PDF";
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string filePath = saveFileDialog.FileName;

                        // Create a new Document
                        Document document = new Document();

                        // Initialize a PdfWriter with the chosen file path
                        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                        // Open the document for writing
                        document.Open();

                        // Create a bold font for the title
                        var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);
                        var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 10, BaseColor.BLACK); // smaller font size and normal weight

                        // Add title to the mark sheet
                        Paragraph title = new Paragraph("Mark Sheet", boldFont);
                        title.SpacingBefore = 20f;
                        title.SpacingAfter = 20f;
                        title.Font.Size = 20;
                        title.Alignment = Element.ALIGN_CENTER;
                        document.Add(title);

                        // Retrieve data from the database
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("SELECT S.RegistrationNo AS [Reg No], MAX(CONCAT(P.FirstName,' ',P.LastName)) AS [Student Name], MAX(Pr.Title) AS [Project Title] " + query + ", SUM((GE.ObtainedMarks * E.TotalWeightage) / E.TotalMarks) AS [Total Marks] FROM GroupEvaluation AS GE JOIN Evaluation AS E ON GE.EvaluationId = E.Id JOIN GroupStudent AS GS ON GS.GroupId = GE.GroupId JOIN Student AS S ON S.Id = GS.StudentId JOIN Person AS P ON P.Id = S.Id JOIN GroupProject AS GP ON GP.GroupId = GE.GroupId JOIN Project AS Pr ON Pr.Id = GP.ProjectId WHERE P.FirstName Not Like '*%' And GS.Status IN (SELECT Id FROM Lookup WHERE Value = 'Active') GROUP BY GE.GroupId, Pr.Id, S.Id, S.RegistrationNo ORDER BY GE.GroupId, Pr.Id, S.RegistrationNo", con);
                        SqlDataReader reader = cmd.ExecuteReader();

                        // Create a table to display the data
                        PdfPTable table = new PdfPTable(reader.FieldCount);
                        table.WidthPercentage = 100;

                        // Add column headers to the table
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i), normalFont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = new BaseColor(128, 128, 128);
                            table.AddCell(cell);
                        }

                        // Add rows to the table
                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                string cellValue = (reader[i] == DBNull.Value) ? "null" : reader[i].ToString(); // Handle DBNull values
                                PdfPCell dataCell = new PdfPCell(new Phrase(cellValue, normalFont)); // smaller font size and normal weight
                                dataCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                table.AddCell(dataCell);
                            }
                        }

                        reader.Close();
                        document.Add(table);

                        // Close the document
                        document.Close();
                        writer.Close();

                        // Show a message box indicating successful generation
                        MessageBox.Show("Mark sheet generated successfully. File saved at: " + filePath);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static List<int> GetEvaluationIds()
        {
            List<int> evaluationIds = new List<int>();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Id FROM Evaluation WHERE Name Not Like '*%'"; 
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    evaluationIds.Add(Convert.ToInt32(reader["Id"]));
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving evaluation IDs: " + ex.Message);
            }

            return evaluationIds;
        }

        public static List<string> GetEvaluationTitle()
        {
            List<string> evaluationTitles = new List<string>();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Name FROM Evaluation WHERE Name Not Like '*%'";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    evaluationTitles.Add(reader["Name"].ToString());
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving evaluation titles: " + ex.Message);
            }

            return evaluationTitles;
        }


        public static void GenerateTopGroupsReport()
    {
        try
        {
            // SQL query to determine top 10 groups based on average marks
            string query = @"SELECT TOP 10 gp.GroupID, AVG(ge.ObtainedMarks) AS AverageMarks
                             FROM GroupEvaluation ge
                             INNER JOIN GroupProject gp ON ge.GroupID = gp.GroupID
                             GROUP BY gp.GroupID
                             ORDER BY AverageMarks DESC";

            // Create a new Document
            Document document = new Document();

            // Initialize a SaveFileDialog to prompt the user for the file path
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PDF files (*.pdf)|*.pdf";
            saveFileDialog.Title = "Save Top 10 Groups Report";

            // Show the SaveFileDialog and wait for the user's response
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog.FileName;

                // Initialize a PdfWriter with the chosen file path
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                // Open the document for writing
                document.Open();

                    // Add a title to the report
                    var boldFont = FontFactory.GetFont(FontFactory.TIMES_ROMAN ,12, BaseColor.BLACK);
                    var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 10, BaseColor.BLACK);
                    Paragraph reportTitle = new Paragraph("Top 10 Groups Report", boldFont);
                reportTitle.SpacingBefore = 20f;
                reportTitle.SpacingAfter = 20f;
                reportTitle.Font.Size = 24;
                reportTitle.Alignment = Element.ALIGN_CENTER;
                document.Add(reportTitle);

                // Execute the SQL query and retrieve data
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                // Create a table to display the data
                PdfPTable table = new PdfPTable(2); // 2 columns for GroupID and AverageMarks
                table.WidthPercentage = 100;

                // Add column headers to the table
                PdfPCell headerCell1 = new PdfPCell(new Phrase("Group ID", boldFont));
                headerCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                headerCell1.BackgroundColor = new BaseColor(128, 128, 128);
                table.AddCell(headerCell1);

                PdfPCell headerCell2 = new PdfPCell(new Phrase("Average Marks", boldFont));
                headerCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                headerCell2.BackgroundColor = new BaseColor(128, 128, 128);
                table.AddCell(headerCell2);

                // Add rows to the table
                while (reader.Read())
                {
                    PdfPCell cell1 = new PdfPCell(new Phrase(reader["GroupID"].ToString()));
                    cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell1);

                    PdfPCell cell2 = new PdfPCell(new Phrase(reader["AverageMarks"].ToString()));
                    cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell2);
                }

                reader.Close();
                document.Add(table);

                // Close the document
                document.Close();
                writer.Close();

                // Show a message box indicating successful generation
                MessageBox.Show("Top 10 Groups report generated successfully. File saved at: " + filePath);
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show("Error generating report: " + ex.Message);
        }
    }

        public static void GenerateStudentGroupChangeReport()
        {
            try
            {
                // SQL query to retrieve data for the report
                string query = @"
                                SELECT FirstName, LastName, OldProjectTitle, NewProjectTitle, 
                                       OldGroupID, NewGroupID, ChangeDate
                                FROM (
                                    SELECT p.FirstName, p.LastName, 
                                           old_pr.Title AS OldProjectTitle, new_pr.Title AS NewProjectTitle,
                                           old_gs.GroupID AS OldGroupID, new_gs.GroupID AS NewGroupID,
                                           old_gp.AssignmentDate AS ChangeDate,
                                           ROW_NUMBER() OVER (PARTITION BY p.FirstName, p.LastName ORDER BY old_gp.AssignmentDate) AS RowNum
                                    FROM Person p
                                    INNER JOIN Student s ON p.ID = s.ID
                                    INNER JOIN GroupStudent old_gs ON s.ID = old_gs.StudentID
                                    INNER JOIN GroupStudent new_gs ON s.ID = new_gs.StudentID
                                    INNER JOIN GroupProject old_gp ON old_gs.GroupID = old_gp.GroupID
                                    INNER JOIN GroupProject new_gp ON new_gs.GroupID = new_gp.GroupID
                                    INNER JOIN Project old_pr ON old_gp.ProjectID = old_pr.ID
                                    INNER JOIN Project new_pr ON new_gp.ProjectID = new_pr.ID
                                    WHERE old_gs.GroupID <> new_gs.GroupID AND p.FirstName NOT LIKE '*%'
                                ) AS Subquery
                                WHERE RowNum = 1";


                // Create a new Document
                Document document = new Document();

                // Initialize a SaveFileDialog to prompt the user for the file path
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF files (*.pdf)|*.pdf";
                saveFileDialog.Title = "Save Student Group Change Report";

                // Show the SaveFileDialog and wait for the user's response
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = saveFileDialog.FileName;

                    // Initialize a PdfWriter with the chosen file path
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                    // Open the document for writing
                    document.Open();

                    // Add a title to the report
                    Font titleFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 18, BaseColor.BLACK);
                    Paragraph title = new Paragraph("Student Group Change Report", titleFont);
                    title.Alignment = Element.ALIGN_CENTER;
                    title.SpacingAfter = 20f;
                    document.Add(title);

                    // Add logo to the report
                    string logoFileName = "logo.png"; // Specify the filename of your logo image
                    string logoPath = Path.Combine(Application.StartupPath, logoFileName); // Combine with the application's startup path
                    if (File.Exists(logoPath))
                    {
                        Image logo = Image.GetInstance(logoPath);
                        logo.Alignment = Element.ALIGN_CENTER;
                        document.Add(logo);
                    }

                    // Execute the SQL query and retrieve data
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(query, con);
                    SqlDataReader reader = cmd.ExecuteReader();

                    // Create a table to display the data
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;

                    // Add column headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = new BaseColor(192, 192, 192); // Light gray background color
                        table.AddCell(cell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(reader[i].ToString()));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }
                    }

                    reader.Close();
                    document.Add(table);

                    // Close the document
                    document.Close();
                    writer.Close();

                    // Show a message box indicating successful generation
                    MessageBox.Show("Student Group Change Report generated successfully. File saved at: " + filePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating report: " + ex.Message);
            }
        }

        public static void GenerateProjectAllocationOverviewReport()
        {
            try
            {
                // SQL query to retrieve project allocation overview data
                string query = @"
        SELECT pr.Title, 
               COALESCE(pr.Description, 'N/A') AS Description, 
               'Pid-' + CAST(gp.GroupId AS VARCHAR) AS GroupIdentifier, 
               COUNT(gs.StudentID) AS GroupSize
        FROM Project pr
        INNER JOIN GroupProject gp ON pr.Id = gp.ProjectId
        INNER JOIN GroupStudent gs ON gp.GroupId = gs.GroupId
        WHERE pr.Title NOT LIKE '*%'
        GROUP BY pr.Title, pr.Description, gp.GroupId";

                // Create a new Document
                Document document = new Document();

                // Initialize a SaveFileDialog to prompt the user for the file path
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF files (*.pdf)|*.pdf";
                saveFileDialog.Title = "Save Project Allocation Overview Report";

                // Show the SaveFileDialog and wait for the user's response
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = saveFileDialog.FileName;

                    // Initialize a PdfWriter with the chosen file path
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                    // Open the document for writing
                    document.Open();

                    // Add a title to the report
                    var titleFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.BLACK);
                    Paragraph title = new Paragraph("Project Allocation Overview Report", titleFont);
                    title.SpacingBefore = 20f;
                    title.SpacingAfter = 20f;
                    title.Alignment = Element.ALIGN_CENTER;
                    document.Add(title);

                    // Execute the SQL query and retrieve data
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(query, con);
                    SqlDataReader reader = cmd.ExecuteReader();

                    // Create a table to display the data
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;

                    // Add column headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell headerCell = new PdfPCell(new Phrase(reader.GetName(i)));
                        headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        headerCell.BackgroundColor = new BaseColor(128, 128, 128);
                        table.AddCell(headerCell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(reader[i].ToString()));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }
                    }

                    reader.Close();
                    document.Add(table);

                    // Close the document
                    document.Close();
                    writer.Close();

                    // Show a message box indicating successful generation
                    MessageBox.Show("Project Allocation Overview Report generated successfully. File saved at: " + filePath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error generating report: " + ex.Message);
            }
        }


    }

}

