﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public class GroupStudent
    {
        public int GroupId { get; set; }
        public int StudentId { get; set; }
        public int Status { get; set; }
        public DateTime AssignmentDate { get; set; }

        
        public GroupStudent(int groupId, int studentId, int status, DateTime assignmentDate)
        {
            GroupId = groupId;
            StudentId = studentId;
            Status = status;
            AssignmentDate = assignmentDate;
        }

        public static bool AddGroupStudent(GroupStudent groupStudent)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "INSERT INTO GroupStudent (GroupId, StudentId, Status, AssignmentDate) " +
                                   "VALUES (@GroupId, @StudentId, @Status, @AssignmentDate)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@GroupId", groupStudent.GroupId);
                    cmd.Parameters.AddWithValue("@StudentId", groupStudent.StudentId);
                    cmd.Parameters.AddWithValue("@Status", groupStudent.Status);
                    cmd.Parameters.AddWithValue("@AssignmentDate", groupStudent.AssignmentDate);
                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }
        public void ViewGroupDetail(int groupId)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                                SELECT s.RegistrationNumber, gs.Status, g.CreatedOn 
                                FROM [Group] g 
                                Left JOIN GroupStudent gs ON g.Id = gs.GroupId
                                JOIN Student s ON gs.StudentId = s.Id
                                WHERE g.Id = @GroupId";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@GroupId", groupId);

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        int id = (int)reader["Id"];
                        DateTime createdOn = (DateTime)reader["CreatedOn"];
                        Console.WriteLine("Group ID: " + id);
                        Console.WriteLine("Created On: " + createdOn);
                    }
                    else
                    {
                        Console.WriteLine("Group not found.");
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error retrieving group details: " + ex.Message);
            }
        }
        public static DataTable ViewGroupDetails()
        {
            DataTable dt = new DataTable();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"SELECT g.Id as GroupId,
                               STRING_AGG(s.RegistrationNo, ', ') as RegisteredStudents,
                               COALESCE(STRING_AGG(CASE WHEN pa.AdvisorRole = 11 THEN p.FirstName + ' ' + p.LastName END, ', '), 'NULL') as Advisors,
                               COALESCE(STRING_AGG(CASE WHEN pa.AdvisorRole = 12 THEN p.FirstName + ' ' + p.LastName END, ', '), 'NULL') as CoAdvisors,
                               COALESCE(STRING_AGG(CASE WHEN pa.AdvisorRole = 14 THEN p.FirstName + ' ' + p.LastName END, ', '), 'NULL') as IndustryAdvisors,
                               COALESCE(pro.Title, 'NULL') as AssignedProject
                                FROM[Group] g
                                LEFT JOIN GroupStudent gs ON g.Id = gs.GroupId
                                LEFT JOIN Student s ON gs.StudentId = s.Id
                                LEFT JOIN GroupProject gp ON g.Id = gp.GroupId
                                LEFT JOIN Project pro ON gp.ProjectId = pro.Id
                                LEFT JOIN ProjectAdvisor pa ON pro.Id = pa.ProjectId
                                LEFT JOIN Advisor a ON pa.AdvisorId = a.Id
                                LEFT JOIN Person p ON a.Id = p.Id
                                Where gs.Status = 3
                                GROUP BY g.Id, g.Created_On, pro.Title";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving group details: " + ex.Message);
            }

            return dt;
        }
        public static bool AddGroupProject(int pid, int gid, DateTime assign)
        {
            try
            {
                    SqlConnection con = Configuration.getInstance().getConnection();
                    string query = "INSERT INTO GroupProject (ProjectId, GroupId, AssignmentDate) " +
                                   "VALUES (@ProjectId, @GroupId, @AssignmentDate)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@ProjectId", pid);
                    cmd.Parameters.AddWithValue("@GroupId", gid);
                    cmd.Parameters.AddWithValue("@AssignmentDate", assign);
                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }

        public static bool RemoveStudentFromGroup(int studentId, int groupId)
        {
            try
            {
                    SqlConnection con = Configuration.getInstance().getConnection();
                    string query = "UPDATE GroupStudent SET Status = 4 WHERE StudentId = @StudentId AND GroupId = @GroupId";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@StudentId", studentId);
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error removing student from group: " + ex.Message);
                return false;
            }
        }


        public static bool UpdateAssignedProject(int groupId, int projectId)
        {
            try
            {
                    SqlConnection con = Configuration.getInstance().getConnection();
                    string updateQuery = "UPDATE GroupProject SET ProjectId = @ProjectId WHERE GroupId = @GroupId";
                    SqlCommand updateCommand = new SqlCommand(updateQuery, con);
                    updateCommand.Parameters.AddWithValue("@ProjectId", projectId);
                    updateCommand.Parameters.AddWithValue("@GroupId", groupId);
                    int rowsAffected = updateCommand.ExecuteNonQuery();
                    if (rowsAffected > 0)
                    {
                        Console.WriteLine("Assigned project updated successfully.");
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Failed to update assigned project.");
                        return false;
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error updating assigned project: " + ex.Message);
                return false;
            }
        }




    }
}
