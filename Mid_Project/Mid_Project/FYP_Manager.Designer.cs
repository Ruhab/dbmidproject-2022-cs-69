﻿
namespace Mid_Project
{
    partial class FYP_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.crud_advisor = new System.Windows.Forms.Button();
            this.Student_CRUD = new System.Windows.Forms.Button();
            this.Evaluations_Crud = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Manage_Reports = new System.Windows.Forms.Button();
            this.Conduct_Evaluations = new System.Windows.Forms.Button();
            this.Assign_Advisor = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.PersonCrud = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.g_female = new System.Windows.Forms.RadioButton();
            this.g_male = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.P_Email = new System.Windows.Forms.TextBox();
            this.P_Contact = new System.Windows.Forms.TextBox();
            this.LastName = new System.Windows.Forms.TextBox();
            this.FirstName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Contact = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.is_Student = new System.Windows.Forms.RadioButton();
            this.update_Student = new System.Windows.Forms.Button();
            this.update_advisor = new System.Windows.Forms.Button();
            this.Add_Person = new System.Windows.Forms.Button();
            this.is_Advisor = new System.Windows.Forms.RadioButton();
            this.Welcome_Page = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StudendCrud = new System.Windows.Forms.TabPage();
            this.View_STU_button = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel10 = new System.Windows.Forms.Panel();
            this.Register_Student = new System.Windows.Forms.Button();
            this.Registration_No = new System.Windows.Forms.TextBox();
            this.Delete_Stu = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.Search_Stu = new System.Windows.Forms.Button();
            this.update_Stu = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.Advisor_Search_TextBox = new System.Windows.Forms.TextBox();
            this.designations = new System.Windows.Forms.ComboBox();
            this.Delete_Advisor = new System.Windows.Forms.Button();
            this.upd_advisor = new System.Windows.Forms.Button();
            this.Search_Advisor = new System.Windows.Forms.Button();
            this.A_Salary = new System.Windows.Forms.TextBox();
            this.Register_advisor = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.View_Advisors_Button = new System.Windows.Forms.Button();
            this.Project_Crud = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.Delete_Project = new System.Windows.Forms.Button();
            this.Search_Project = new System.Windows.Forms.Button();
            this.update_Project = new System.Windows.Forms.Button();
            this.Add_Project = new System.Windows.Forms.Button();
            this.p_description = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.p_Title = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.View_Projects = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.View_Eval = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.panel11 = new System.Windows.Forms.Panel();
            this.Delete_Eval = new System.Windows.Forms.Button();
            this.Search_Eval = new System.Windows.Forms.Button();
            this.Upd_Eval = new System.Windows.Forms.Button();
            this.E_Weightage = new System.Windows.Forms.TextBox();
            this.E_Marks = new System.Windows.Forms.TextBox();
            this.E_Name = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Add_Eval = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel15 = new System.Windows.Forms.Panel();
            this.View_Groups = new System.Windows.Forms.Button();
            this.groups_list = new System.Windows.Forms.DataGridView();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.UpdateGroup = new System.Windows.Forms.Button();
            this.create_Group = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.SpecificGroupStu = new System.Windows.Forms.DataGridView();
            this.panel16 = new System.Windows.Forms.Panel();
            this.AddNewStu = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.UpdateAssignedPro = new System.Windows.Forms.Button();
            this.DelGroupStu = new System.Windows.Forms.Button();
            this.Add_Group_Stu = new System.Windows.Forms.Button();
            this.Assign_Project = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Student_list = new System.Windows.Forms.ComboBox();
            this.projects_list = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.Load_All_Projects = new System.Windows.Forms.Button();
            this.Load_Distinct_Pro = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.Add_adv = new System.Windows.Forms.Button();
            this.Add_co_adv = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.Add_Ind_adv = new System.Windows.Forms.Button();
            this.adv_projects = new System.Windows.Forms.ComboBox();
            this.adv_list = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.co_adv_list = new System.Windows.Forms.ComboBox();
            this.ind_adv_list = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Evaluate_Group = new System.Windows.Forms.TabPage();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.panel19 = new System.Windows.Forms.Panel();
            this.Save_upd_evl = new System.Windows.Forms.Button();
            this.upd_evl_grp = new System.Windows.Forms.Button();
            this.add_evaluate_group = new System.Windows.Forms.Button();
            this.evaluations_list = new System.Windows.Forms.ComboBox();
            this.Total_marks_label = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.Obtained_Marks = new System.Windows.Forms.TextBox();
            this.group_ids_list = new System.Windows.Forms.ComboBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.ProjectAllocationReport = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.student_who_changed_group = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.top10groups = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.Generate_Mark_Sheet = new System.Windows.Forms.Button();
            this.advisory_board_report = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Back = new System.Windows.Forms.Button();
            this.Add_Persons = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.PersonCrud.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel13.SuspendLayout();
            this.Welcome_Page.SuspendLayout();
            this.panel4.SuspendLayout();
            this.StudendCrud.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel10.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.Project_Crud.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel11.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groups_list)).BeginInit();
            this.panel14.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpecificGroupStu)).BeginInit();
            this.panel16.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel17.SuspendLayout();
            this.Evaluate_Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.panel19.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 188);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MintCream;
            this.label1.Location = new System.Drawing.Point(255, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(410, 65);
            this.label1.TabIndex = 0;
            this.label1.Text = "FYP Manager";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.crud_advisor);
            this.panel2.Controls.Add(this.Student_CRUD);
            this.panel2.Controls.Add(this.Add_Persons);
            this.panel2.Controls.Add(this.Evaluations_Crud);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.Manage_Reports);
            this.panel2.Controls.Add(this.Conduct_Evaluations);
            this.panel2.Controls.Add(this.Assign_Advisor);
            this.panel2.Location = new System.Drawing.Point(0, 186);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 495);
            this.panel2.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label29.Location = new System.Drawing.Point(3, 404);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(156, 22);
            this.label29.TabIndex = 12;
            this.label29.Text = "Generate Reports";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label28.Location = new System.Drawing.Point(3, 268);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(101, 25);
            this.label28.TabIndex = 11;
            this.label28.Text = "Evaluate";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("Century", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label23.Location = new System.Drawing.Point(3, 5);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 27);
            this.label23.TabIndex = 10;
            this.label23.Text = "Manage";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Teal;
            this.button1.Location = new System.Drawing.Point(19, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 26);
            this.button1.TabIndex = 3;
            this.button1.Text = "Manage Projects";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // crud_advisor
            // 
            this.crud_advisor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.crud_advisor.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crud_advisor.ForeColor = System.Drawing.Color.Teal;
            this.crud_advisor.Location = new System.Drawing.Point(19, 117);
            this.crud_advisor.Name = "crud_advisor";
            this.crud_advisor.Size = new System.Drawing.Size(140, 26);
            this.crud_advisor.TabIndex = 2;
            this.crud_advisor.Text = "Manage Advisor";
            this.crud_advisor.UseVisualStyleBackColor = true;
            this.crud_advisor.Click += new System.EventHandler(this.crud_advisor_Click);
            // 
            // Student_CRUD
            // 
            this.Student_CRUD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Student_CRUD.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Student_CRUD.ForeColor = System.Drawing.Color.Teal;
            this.Student_CRUD.Location = new System.Drawing.Point(19, 85);
            this.Student_CRUD.Name = "Student_CRUD";
            this.Student_CRUD.Size = new System.Drawing.Size(140, 26);
            this.Student_CRUD.TabIndex = 1;
            this.Student_CRUD.Text = "Manage Student";
            this.Student_CRUD.UseVisualStyleBackColor = true;
            this.Student_CRUD.Click += new System.EventHandler(this.Student_CRUD_Click);
            // 
            // Evaluations_Crud
            // 
            this.Evaluations_Crud.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Evaluations_Crud.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Evaluations_Crud.ForeColor = System.Drawing.Color.Teal;
            this.Evaluations_Crud.Location = new System.Drawing.Point(19, 213);
            this.Evaluations_Crud.Name = "Evaluations_Crud";
            this.Evaluations_Crud.Size = new System.Drawing.Size(140, 26);
            this.Evaluations_Crud.TabIndex = 6;
            this.Evaluations_Crud.Text = "Manage Evaluations";
            this.Evaluations_Crud.UseVisualStyleBackColor = true;
            this.Evaluations_Crud.Click += new System.EventHandler(this.Evaluations_Crud_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Teal;
            this.button2.Location = new System.Drawing.Point(19, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 26);
            this.button2.TabIndex = 4;
            this.button2.Text = "Manage Group";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Manage_Reports
            // 
            this.Manage_Reports.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Manage_Reports.ForeColor = System.Drawing.Color.Teal;
            this.Manage_Reports.Location = new System.Drawing.Point(36, 446);
            this.Manage_Reports.Name = "Manage_Reports";
            this.Manage_Reports.Size = new System.Drawing.Size(115, 26);
            this.Manage_Reports.TabIndex = 9;
            this.Manage_Reports.Text = "Reports";
            this.Manage_Reports.UseVisualStyleBackColor = true;
            this.Manage_Reports.Click += new System.EventHandler(this.Manage_Reports_Click);
            // 
            // Conduct_Evaluations
            // 
            this.Conduct_Evaluations.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Conduct_Evaluations.ForeColor = System.Drawing.Color.Teal;
            this.Conduct_Evaluations.Location = new System.Drawing.Point(36, 348);
            this.Conduct_Evaluations.Name = "Conduct_Evaluations";
            this.Conduct_Evaluations.Size = new System.Drawing.Size(115, 26);
            this.Conduct_Evaluations.TabIndex = 8;
            this.Conduct_Evaluations.Text = "Conduct Evaluation";
            this.Conduct_Evaluations.UseVisualStyleBackColor = true;
            this.Conduct_Evaluations.Click += new System.EventHandler(this.Conduct_Evaluations_Click);
            // 
            // Assign_Advisor
            // 
            this.Assign_Advisor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Assign_Advisor.ForeColor = System.Drawing.Color.Teal;
            this.Assign_Advisor.Location = new System.Drawing.Point(36, 316);
            this.Assign_Advisor.Name = "Assign_Advisor";
            this.Assign_Advisor.Size = new System.Drawing.Size(115, 26);
            this.Assign_Advisor.TabIndex = 7;
            this.Assign_Advisor.Text = "Assign Advisors";
            this.Assign_Advisor.UseVisualStyleBackColor = true;
            this.Assign_Advisor.Click += new System.EventHandler(this.Assign_Advisor_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.PersonCrud);
            this.tabControl1.Controls.Add(this.Welcome_Page);
            this.tabControl1.Controls.Add(this.StudendCrud);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.Project_Crud);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.Evaluate_Group);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(0, -22);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(618, 517);
            this.tabControl1.TabIndex = 2;
            // 
            // PersonCrud
            // 
            this.PersonCrud.BackColor = System.Drawing.Color.Honeydew;
            this.PersonCrud.Controls.Add(this.panel3);
            this.PersonCrud.Controls.Add(this.panel13);
            this.PersonCrud.Location = new System.Drawing.Point(4, 22);
            this.PersonCrud.Name = "PersonCrud";
            this.PersonCrud.Padding = new System.Windows.Forms.Padding(3);
            this.PersonCrud.Size = new System.Drawing.Size(610, 491);
            this.PersonCrud.TabIndex = 0;
            this.PersonCrud.Text = "Person Crud";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.BackColor = System.Drawing.Color.Snow;
            this.panel3.Controls.Add(this.g_female);
            this.panel3.Controls.Add(this.g_male);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.dateTimePicker1);
            this.panel3.Controls.Add(this.P_Email);
            this.panel3.Controls.Add(this.P_Contact);
            this.panel3.Controls.Add(this.LastName);
            this.panel3.Controls.Add(this.FirstName);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.Contact);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(68, 60);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(518, 289);
            this.panel3.TabIndex = 0;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // g_female
            // 
            this.g_female.AutoSize = true;
            this.g_female.Location = new System.Drawing.Point(254, 173);
            this.g_female.Name = "g_female";
            this.g_female.Size = new System.Drawing.Size(59, 17);
            this.g_female.TabIndex = 11;
            this.g_female.TabStop = true;
            this.g_female.Text = "Female";
            this.g_female.UseVisualStyleBackColor = true;
            // 
            // g_male
            // 
            this.g_male.AutoSize = true;
            this.g_male.Location = new System.Drawing.Point(137, 173);
            this.g_male.Name = "g_male";
            this.g_male.Size = new System.Drawing.Size(48, 17);
            this.g_male.TabIndex = 10;
            this.g_male.TabStop = true;
            this.g_male.Text = "Male";
            this.g_male.UseVisualStyleBackColor = true;
            this.g_male.CheckedChanged += new System.EventHandler(this.g_male_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(19, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "Gender";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(137, 211);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // P_Email
            // 
            this.P_Email.Location = new System.Drawing.Point(137, 134);
            this.P_Email.Name = "P_Email";
            this.P_Email.Size = new System.Drawing.Size(324, 20);
            this.P_Email.TabIndex = 8;
            this.P_Email.TextChanged += new System.EventHandler(this.P_Email_TextChanged);
            // 
            // P_Contact
            // 
            this.P_Contact.Location = new System.Drawing.Point(137, 87);
            this.P_Contact.Name = "P_Contact";
            this.P_Contact.Size = new System.Drawing.Size(324, 20);
            this.P_Contact.TabIndex = 7;
            // 
            // LastName
            // 
            this.LastName.Location = new System.Drawing.Point(137, 52);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(324, 20);
            this.LastName.TabIndex = 6;
            // 
            // FirstName
            // 
            this.FirstName.Location = new System.Drawing.Point(137, 14);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(324, 20);
            this.FirstName.TabIndex = 5;
            this.FirstName.TextChanged += new System.EventHandler(this.FirstName_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Window;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(19, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Date of Birth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(19, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Email";
            // 
            // Contact
            // 
            this.Contact.AutoSize = true;
            this.Contact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contact.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Contact.Location = new System.Drawing.Point(19, 91);
            this.Contact.Name = "Contact";
            this.Contact.Size = new System.Drawing.Size(53, 16);
            this.Contact.TabIndex = 2;
            this.Contact.Text = "Contact";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(16, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Last Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(16, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "First Name";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel13
            // 
            this.panel13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel13.Controls.Add(this.is_Student);
            this.panel13.Controls.Add(this.update_Student);
            this.panel13.Controls.Add(this.update_advisor);
            this.panel13.Controls.Add(this.Add_Person);
            this.panel13.Controls.Add(this.is_Advisor);
            this.panel13.Location = new System.Drawing.Point(316, 355);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(270, 130);
            this.panel13.TabIndex = 7;
            // 
            // is_Student
            // 
            this.is_Student.AutoSize = true;
            this.is_Student.Location = new System.Drawing.Point(44, 3);
            this.is_Student.Name = "is_Student";
            this.is_Student.Size = new System.Drawing.Size(62, 17);
            this.is_Student.TabIndex = 6;
            this.is_Student.TabStop = true;
            this.is_Student.Text = "Student";
            this.is_Student.UseVisualStyleBackColor = true;
            // 
            // update_Student
            // 
            this.update_Student.BackColor = System.Drawing.Color.LightGreen;
            this.update_Student.Location = new System.Drawing.Point(190, 50);
            this.update_Student.Name = "update_Student";
            this.update_Student.Size = new System.Drawing.Size(75, 23);
            this.update_Student.TabIndex = 4;
            this.update_Student.Text = "Update";
            this.update_Student.UseVisualStyleBackColor = false;
            this.update_Student.Click += new System.EventHandler(this.update_Student_Click);
            // 
            // update_advisor
            // 
            this.update_advisor.BackColor = System.Drawing.Color.LightGreen;
            this.update_advisor.Location = new System.Drawing.Point(96, 71);
            this.update_advisor.Name = "update_advisor";
            this.update_advisor.Size = new System.Drawing.Size(75, 23);
            this.update_advisor.TabIndex = 5;
            this.update_advisor.Text = "Update";
            this.update_advisor.UseVisualStyleBackColor = false;
            this.update_advisor.Click += new System.EventHandler(this.update_advisor_Click);
            // 
            // Add_Person
            // 
            this.Add_Person.Location = new System.Drawing.Point(192, 96);
            this.Add_Person.Name = "Add_Person";
            this.Add_Person.Size = new System.Drawing.Size(75, 23);
            this.Add_Person.TabIndex = 3;
            this.Add_Person.Text = "Add";
            this.Add_Person.UseVisualStyleBackColor = true;
            this.Add_Person.Click += new System.EventHandler(this.Add_Person_Click);
            // 
            // is_Advisor
            // 
            this.is_Advisor.AutoSize = true;
            this.is_Advisor.Location = new System.Drawing.Point(154, 3);
            this.is_Advisor.Name = "is_Advisor";
            this.is_Advisor.Size = new System.Drawing.Size(60, 17);
            this.is_Advisor.TabIndex = 2;
            this.is_Advisor.Text = "Advisor";
            this.is_Advisor.UseVisualStyleBackColor = true;
            // 
            // Welcome_Page
            // 
            this.Welcome_Page.BackColor = System.Drawing.Color.AliceBlue;
            this.Welcome_Page.Controls.Add(this.panel4);
            this.Welcome_Page.Location = new System.Drawing.Point(4, 22);
            this.Welcome_Page.Name = "Welcome_Page";
            this.Welcome_Page.Padding = new System.Windows.Forms.Padding(3);
            this.Welcome_Page.Size = new System.Drawing.Size(610, 491);
            this.Welcome_Page.TabIndex = 1;
            this.Welcome_Page.Text = "Welcome_Page";
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel4.BackColor = System.Drawing.Color.Teal;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(72, 92);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(499, 279);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(52, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(416, 88);
            this.label7.TabIndex = 1;
            this.label7.Text = "To the  task  ease  platform. Your go-to  project \r\nmanagement for final year FYP" +
    "! Stay organized,\r\ncollaborate  efficiently, and   ace your   projects \r\nwith ea" +
    "se.";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Cambria", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(153, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(226, 57);
            this.label6.TabIndex = 0;
            this.label6.Text = "Welcome";
            // 
            // StudendCrud
            // 
            this.StudendCrud.BackColor = System.Drawing.Color.MintCream;
            this.StudendCrud.Controls.Add(this.View_STU_button);
            this.StudendCrud.Controls.Add(this.dataGridView1);
            this.StudendCrud.Controls.Add(this.panel10);
            this.StudendCrud.Location = new System.Drawing.Point(4, 22);
            this.StudendCrud.Name = "StudendCrud";
            this.StudendCrud.Padding = new System.Windows.Forms.Padding(3);
            this.StudendCrud.Size = new System.Drawing.Size(610, 491);
            this.StudendCrud.TabIndex = 2;
            this.StudendCrud.Text = "Student CRUD";
            // 
            // View_STU_button
            // 
            this.View_STU_button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.View_STU_button.BackColor = System.Drawing.Color.WhiteSmoke;
            this.View_STU_button.ForeColor = System.Drawing.Color.DarkGreen;
            this.View_STU_button.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.View_STU_button.Location = new System.Drawing.Point(518, 451);
            this.View_STU_button.Name = "View_STU_button";
            this.View_STU_button.Size = new System.Drawing.Size(84, 23);
            this.View_STU_button.TabIndex = 12;
            this.View_STU_button.Text = "View Students";
            this.View_STU_button.UseVisualStyleBackColor = false;
            this.View_STU_button.Click += new System.EventHandler(this.View_STU_button_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 156);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(593, 287);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // panel10
            // 
            this.panel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel10.BackColor = System.Drawing.Color.Snow;
            this.panel10.Controls.Add(this.Register_Student);
            this.panel10.Controls.Add(this.Registration_No);
            this.panel10.Controls.Add(this.Delete_Stu);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.Search_Stu);
            this.panel10.Controls.Add(this.update_Stu);
            this.panel10.Location = new System.Drawing.Point(16, 17);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(597, 133);
            this.panel10.TabIndex = 13;
            // 
            // Register_Student
            // 
            this.Register_Student.BackColor = System.Drawing.Color.GreenYellow;
            this.Register_Student.Location = new System.Drawing.Point(254, 101);
            this.Register_Student.Name = "Register_Student";
            this.Register_Student.Size = new System.Drawing.Size(75, 23);
            this.Register_Student.TabIndex = 8;
            this.Register_Student.Text = "Register";
            this.Register_Student.UseVisualStyleBackColor = false;
            this.Register_Student.Click += new System.EventHandler(this.Register_Student_Click);
            // 
            // Registration_No
            // 
            this.Registration_No.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Registration_No.Location = new System.Drawing.Point(203, 54);
            this.Registration_No.Name = "Registration_No";
            this.Registration_No.Size = new System.Drawing.Size(324, 20);
            this.Registration_No.TabIndex = 7;
            // 
            // Delete_Stu
            // 
            this.Delete_Stu.BackColor = System.Drawing.Color.IndianRed;
            this.Delete_Stu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Delete_Stu.Location = new System.Drawing.Point(518, 101);
            this.Delete_Stu.Name = "Delete_Stu";
            this.Delete_Stu.Size = new System.Drawing.Size(75, 23);
            this.Delete_Stu.TabIndex = 9;
            this.Delete_Stu.Text = "Delete ";
            this.Delete_Stu.UseVisualStyleBackColor = false;
            this.Delete_Stu.Click += new System.EventHandler(this.Delete_Stu_Click);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(31, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 16);
            this.label9.TabIndex = 6;
            this.label9.Text = "Registration Number";
            // 
            // Search_Stu
            // 
            this.Search_Stu.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.Search_Stu.Location = new System.Drawing.Point(426, 101);
            this.Search_Stu.Name = "Search_Stu";
            this.Search_Stu.Size = new System.Drawing.Size(75, 23);
            this.Search_Stu.TabIndex = 10;
            this.Search_Stu.Text = "Search";
            this.Search_Stu.UseVisualStyleBackColor = false;
            this.Search_Stu.Click += new System.EventHandler(this.Search_Stu_Click);
            // 
            // update_Stu
            // 
            this.update_Stu.BackColor = System.Drawing.Color.Orange;
            this.update_Stu.Location = new System.Drawing.Point(345, 101);
            this.update_Stu.Name = "update_Stu";
            this.update_Stu.Size = new System.Drawing.Size(75, 23);
            this.update_Stu.TabIndex = 11;
            this.update_Stu.Text = "Update";
            this.update_Stu.UseVisualStyleBackColor = false;
            this.update_Stu.Click += new System.EventHandler(this.update_Stu_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.MintCream;
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(610, 491);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Advisor_Crud";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Maroon;
            this.label17.Location = new System.Drawing.Point(0, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 19);
            this.label17.TabIndex = 21;
            this.label17.Text = "NOTE:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Green;
            this.label14.Location = new System.Drawing.Point(66, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(542, 30);
            this.label14.TabIndex = 20;
            this.label14.Text = "In order to update advisor, select the row you want to update. Enter updated desi" +
    "gnation and Salary\r\nand then click \"Update\".";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // panel8
            // 
            this.panel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel8.BackColor = System.Drawing.Color.Ivory;
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.Advisor_Search_TextBox);
            this.panel8.Controls.Add(this.designations);
            this.panel8.Controls.Add(this.Delete_Advisor);
            this.panel8.Controls.Add(this.upd_advisor);
            this.panel8.Controls.Add(this.Search_Advisor);
            this.panel8.Controls.Add(this.A_Salary);
            this.panel8.Controls.Add(this.Register_advisor);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Location = new System.Drawing.Point(-13, 11);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(625, 153);
            this.panel8.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FloralWhite;
            this.label12.Location = new System.Drawing.Point(407, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Enter your search text here";
            // 
            // Advisor_Search_TextBox
            // 
            this.Advisor_Search_TextBox.Location = new System.Drawing.Point(410, 86);
            this.Advisor_Search_TextBox.Name = "Advisor_Search_TextBox";
            this.Advisor_Search_TextBox.Size = new System.Drawing.Size(207, 20);
            this.Advisor_Search_TextBox.TabIndex = 18;
            // 
            // designations
            // 
            this.designations.FormattingEnabled = true;
            this.designations.Items.AddRange(new object[] {
            "Professor",
            "Associate Professor",
            "Assistant Professor",
            "Lecturer",
            "Industry Professional"});
            this.designations.Location = new System.Drawing.Point(111, 50);
            this.designations.Name = "designations";
            this.designations.Size = new System.Drawing.Size(273, 21);
            this.designations.TabIndex = 16;
            this.designations.SelectedIndexChanged += new System.EventHandler(this.designations_SelectedIndexChanged);
            // 
            // Delete_Advisor
            // 
            this.Delete_Advisor.BackColor = System.Drawing.Color.IndianRed;
            this.Delete_Advisor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Delete_Advisor.Location = new System.Drawing.Point(309, 122);
            this.Delete_Advisor.Name = "Delete_Advisor";
            this.Delete_Advisor.Size = new System.Drawing.Size(75, 23);
            this.Delete_Advisor.TabIndex = 13;
            this.Delete_Advisor.Text = "Delete ";
            this.Delete_Advisor.UseVisualStyleBackColor = false;
            this.Delete_Advisor.Click += new System.EventHandler(this.Delete_Advisor_Click);
            // 
            // upd_advisor
            // 
            this.upd_advisor.BackColor = System.Drawing.Color.Orange;
            this.upd_advisor.Location = new System.Drawing.Point(219, 122);
            this.upd_advisor.Name = "upd_advisor";
            this.upd_advisor.Size = new System.Drawing.Size(75, 23);
            this.upd_advisor.TabIndex = 15;
            this.upd_advisor.Text = "Update";
            this.upd_advisor.UseVisualStyleBackColor = false;
            this.upd_advisor.Click += new System.EventHandler(this.upd_advisor_Click);
            // 
            // Search_Advisor
            // 
            this.Search_Advisor.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.Search_Advisor.Location = new System.Drawing.Point(542, 122);
            this.Search_Advisor.Name = "Search_Advisor";
            this.Search_Advisor.Size = new System.Drawing.Size(75, 23);
            this.Search_Advisor.TabIndex = 14;
            this.Search_Advisor.Text = "Search";
            this.Search_Advisor.UseVisualStyleBackColor = false;
            this.Search_Advisor.Click += new System.EventHandler(this.Search_Advisor_Click);
            // 
            // A_Salary
            // 
            this.A_Salary.Location = new System.Drawing.Point(111, 96);
            this.A_Salary.Name = "A_Salary";
            this.A_Salary.Size = new System.Drawing.Size(273, 20);
            this.A_Salary.TabIndex = 4;
            this.A_Salary.TextChanged += new System.EventHandler(this.A_Salary_TextChanged);
            // 
            // Register_advisor
            // 
            this.Register_advisor.BackColor = System.Drawing.Color.GreenYellow;
            this.Register_advisor.Location = new System.Drawing.Point(124, 122);
            this.Register_advisor.Name = "Register_advisor";
            this.Register_advisor.Size = new System.Drawing.Size(75, 23);
            this.Register_advisor.TabIndex = 12;
            this.Register_advisor.Text = "Register";
            this.Register_advisor.UseVisualStyleBackColor = false;
            this.Register_advisor.Click += new System.EventHandler(this.Register_advisor_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Salary $";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Designation";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // panel9
            // 
            this.panel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel9.Controls.Add(this.dataGridView2);
            this.panel9.Controls.Add(this.View_Advisors_Button);
            this.panel9.Location = new System.Drawing.Point(-1, 162);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(613, 317);
            this.panel9.TabIndex = 23;
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(31, 8);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(543, 275);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // View_Advisors_Button
            // 
            this.View_Advisors_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.View_Advisors_Button.BackColor = System.Drawing.Color.GhostWhite;
            this.View_Advisors_Button.ForeColor = System.Drawing.Color.Green;
            this.View_Advisors_Button.Location = new System.Drawing.Point(512, 289);
            this.View_Advisors_Button.Name = "View_Advisors_Button";
            this.View_Advisors_Button.Size = new System.Drawing.Size(91, 23);
            this.View_Advisors_Button.TabIndex = 17;
            this.View_Advisors_Button.Text = "View Advisors";
            this.View_Advisors_Button.UseVisualStyleBackColor = false;
            this.View_Advisors_Button.Click += new System.EventHandler(this.View_Advisors_Button_Click);
            // 
            // Project_Crud
            // 
            this.Project_Crud.BackColor = System.Drawing.Color.Honeydew;
            this.Project_Crud.Controls.Add(this.panel6);
            this.Project_Crud.Controls.Add(this.panel7);
            this.Project_Crud.Location = new System.Drawing.Point(4, 22);
            this.Project_Crud.Name = "Project_Crud";
            this.Project_Crud.Padding = new System.Windows.Forms.Padding(3);
            this.Project_Crud.Size = new System.Drawing.Size(610, 491);
            this.Project_Crud.TabIndex = 4;
            this.Project_Crud.Text = "project_Crud";
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel6.BackColor = System.Drawing.Color.Snow;
            this.panel6.Controls.Add(this.Delete_Project);
            this.panel6.Controls.Add(this.Search_Project);
            this.panel6.Controls.Add(this.update_Project);
            this.panel6.Controls.Add(this.Add_Project);
            this.panel6.Controls.Add(this.p_description);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.p_Title);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Location = new System.Drawing.Point(43, 17);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(526, 216);
            this.panel6.TabIndex = 0;
            // 
            // Delete_Project
            // 
            this.Delete_Project.BackColor = System.Drawing.Color.IndianRed;
            this.Delete_Project.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Delete_Project.Location = new System.Drawing.Point(362, 181);
            this.Delete_Project.Name = "Delete_Project";
            this.Delete_Project.Size = new System.Drawing.Size(78, 23);
            this.Delete_Project.TabIndex = 9;
            this.Delete_Project.Text = "Delete";
            this.Delete_Project.UseVisualStyleBackColor = false;
            this.Delete_Project.Click += new System.EventHandler(this.Delete_Project_Click);
            // 
            // Search_Project
            // 
            this.Search_Project.BackColor = System.Drawing.Color.Beige;
            this.Search_Project.Location = new System.Drawing.Point(246, 181);
            this.Search_Project.Name = "Search_Project";
            this.Search_Project.Size = new System.Drawing.Size(75, 23);
            this.Search_Project.TabIndex = 8;
            this.Search_Project.Text = "Search";
            this.Search_Project.UseVisualStyleBackColor = false;
            this.Search_Project.Click += new System.EventHandler(this.Search_Project_Click);
            // 
            // update_Project
            // 
            this.update_Project.BackColor = System.Drawing.Color.LightYellow;
            this.update_Project.Location = new System.Drawing.Point(135, 178);
            this.update_Project.Name = "update_Project";
            this.update_Project.Size = new System.Drawing.Size(75, 23);
            this.update_Project.TabIndex = 7;
            this.update_Project.Text = "Update";
            this.update_Project.UseVisualStyleBackColor = false;
            this.update_Project.Click += new System.EventHandler(this.update_Project_Click);
            // 
            // Add_Project
            // 
            this.Add_Project.BackColor = System.Drawing.Color.PaleGreen;
            this.Add_Project.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Add_Project.Location = new System.Drawing.Point(24, 178);
            this.Add_Project.Name = "Add_Project";
            this.Add_Project.Size = new System.Drawing.Size(75, 23);
            this.Add_Project.TabIndex = 6;
            this.Add_Project.Text = "Add";
            this.Add_Project.UseVisualStyleBackColor = false;
            this.Add_Project.Click += new System.EventHandler(this.Add_Project_Click);
            // 
            // p_description
            // 
            this.p_description.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.p_description.Location = new System.Drawing.Point(166, 92);
            this.p_description.Multiline = true;
            this.p_description.Name = "p_description";
            this.p_description.Size = new System.Drawing.Size(274, 64);
            this.p_description.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 99);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(138, 18);
            this.label16.TabIndex = 4;
            this.label16.Text = "Project Description:";
            // 
            // p_Title
            // 
            this.p_Title.Location = new System.Drawing.Point(166, 59);
            this.p_Title.Name = "p_Title";
            this.p_Title.Size = new System.Drawing.Size(274, 20);
            this.p_Title.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Teal;
            this.label15.Location = new System.Drawing.Point(172, 12);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(216, 34);
            this.label15.TabIndex = 2;
            this.label15.Text = "Project Details";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 18);
            this.label13.TabIndex = 0;
            this.label13.Text = "Project Title:";
            // 
            // panel7
            // 
            this.panel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel7.BackColor = System.Drawing.Color.Snow;
            this.panel7.Controls.Add(this.dataGridView3);
            this.panel7.Controls.Add(this.View_Projects);
            this.panel7.Location = new System.Drawing.Point(14, 239);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(593, 240);
            this.panel7.TabIndex = 3;
            // 
            // dataGridView3
            // 
            this.dataGridView3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(29, 18);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(545, 191);
            this.dataGridView3.TabIndex = 1;
            // 
            // View_Projects
            // 
            this.View_Projects.Location = new System.Drawing.Point(513, 215);
            this.View_Projects.Name = "View_Projects";
            this.View_Projects.Size = new System.Drawing.Size(75, 23);
            this.View_Projects.TabIndex = 2;
            this.View_Projects.Text = "View";
            this.View_Projects.UseVisualStyleBackColor = true;
            this.View_Projects.Click += new System.EventHandler(this.View_Projects_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage2.Controls.Add(this.panel12);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(610, 491);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Evaluations Crud";
            // 
            // panel12
            // 
            this.panel12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel12.Controls.Add(this.View_Eval);
            this.panel12.Controls.Add(this.dataGridView4);
            this.panel12.Location = new System.Drawing.Point(38, 185);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(544, 312);
            this.panel12.TabIndex = 0;
            // 
            // View_Eval
            // 
            this.View_Eval.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.View_Eval.BackColor = System.Drawing.Color.Teal;
            this.View_Eval.ForeColor = System.Drawing.Color.White;
            this.View_Eval.Location = new System.Drawing.Point(456, 276);
            this.View_Eval.Name = "View_Eval";
            this.View_Eval.Size = new System.Drawing.Size(75, 23);
            this.View_Eval.TabIndex = 10;
            this.View_Eval.Text = "View";
            this.View_Eval.UseVisualStyleBackColor = false;
            this.View_Eval.Click += new System.EventHandler(this.View_Eval_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(30, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(476, 275);
            this.dataGridView4.TabIndex = 0;
            // 
            // panel11
            // 
            this.panel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel11.BackColor = System.Drawing.Color.Snow;
            this.panel11.Controls.Add(this.Delete_Eval);
            this.panel11.Controls.Add(this.Search_Eval);
            this.panel11.Controls.Add(this.Upd_Eval);
            this.panel11.Controls.Add(this.E_Weightage);
            this.panel11.Controls.Add(this.E_Marks);
            this.panel11.Controls.Add(this.E_Name);
            this.panel11.Controls.Add(this.label20);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Controls.Add(this.label18);
            this.panel11.Controls.Add(this.Add_Eval);
            this.panel11.Location = new System.Drawing.Point(68, 17);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(476, 162);
            this.panel11.TabIndex = 0;
            // 
            // Delete_Eval
            // 
            this.Delete_Eval.BackColor = System.Drawing.Color.Crimson;
            this.Delete_Eval.ForeColor = System.Drawing.Color.White;
            this.Delete_Eval.Location = new System.Drawing.Point(389, 114);
            this.Delete_Eval.Name = "Delete_Eval";
            this.Delete_Eval.Size = new System.Drawing.Size(75, 23);
            this.Delete_Eval.TabIndex = 9;
            this.Delete_Eval.Text = "Delete";
            this.Delete_Eval.UseVisualStyleBackColor = false;
            this.Delete_Eval.Click += new System.EventHandler(this.Delete_Eval_Click);
            // 
            // Search_Eval
            // 
            this.Search_Eval.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Search_Eval.ForeColor = System.Drawing.Color.White;
            this.Search_Eval.Location = new System.Drawing.Point(308, 114);
            this.Search_Eval.Name = "Search_Eval";
            this.Search_Eval.Size = new System.Drawing.Size(75, 23);
            this.Search_Eval.TabIndex = 8;
            this.Search_Eval.Text = "Search";
            this.Search_Eval.UseVisualStyleBackColor = false;
            this.Search_Eval.Click += new System.EventHandler(this.Search_Eval_Click);
            // 
            // Upd_Eval
            // 
            this.Upd_Eval.BackColor = System.Drawing.Color.Khaki;
            this.Upd_Eval.Location = new System.Drawing.Point(227, 114);
            this.Upd_Eval.Name = "Upd_Eval";
            this.Upd_Eval.Size = new System.Drawing.Size(75, 23);
            this.Upd_Eval.TabIndex = 7;
            this.Upd_Eval.Text = "Update";
            this.Upd_Eval.UseVisualStyleBackColor = false;
            this.Upd_Eval.Click += new System.EventHandler(this.Upd_Eval_Click);
            // 
            // E_Weightage
            // 
            this.E_Weightage.Location = new System.Drawing.Point(135, 80);
            this.E_Weightage.Name = "E_Weightage";
            this.E_Weightage.Size = new System.Drawing.Size(304, 20);
            this.E_Weightage.TabIndex = 6;
            // 
            // E_Marks
            // 
            this.E_Marks.Location = new System.Drawing.Point(135, 49);
            this.E_Marks.Name = "E_Marks";
            this.E_Marks.Size = new System.Drawing.Size(304, 20);
            this.E_Marks.TabIndex = 5;
            // 
            // E_Name
            // 
            this.E_Name.Location = new System.Drawing.Point(135, 23);
            this.E_Name.Name = "E_Name";
            this.E_Name.Size = new System.Drawing.Size(304, 20);
            this.E_Name.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(25, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Total Marks";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(25, 83);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Total Weightage";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(25, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Name";
            // 
            // Add_Eval
            // 
            this.Add_Eval.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Add_Eval.Location = new System.Drawing.Point(146, 114);
            this.Add_Eval.Name = "Add_Eval";
            this.Add_Eval.Size = new System.Drawing.Size(75, 23);
            this.Add_Eval.TabIndex = 0;
            this.Add_Eval.Text = "Add";
            this.Add_Eval.UseVisualStyleBackColor = false;
            this.Add_Eval.Click += new System.EventHandler(this.Add_Eval_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage3.Controls.Add(this.panel15);
            this.tabPage3.Controls.Add(this.panel14);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(610, 491);
            this.tabPage3.TabIndex = 6;
            this.tabPage3.Text = "Group_Crud";
            // 
            // panel15
            // 
            this.panel15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel15.Controls.Add(this.View_Groups);
            this.panel15.Controls.Add(this.groups_list);
            this.panel15.Location = new System.Drawing.Point(45, 159);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(544, 334);
            this.panel15.TabIndex = 2;
            // 
            // View_Groups
            // 
            this.View_Groups.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.View_Groups.Location = new System.Drawing.Point(442, 3);
            this.View_Groups.Name = "View_Groups";
            this.View_Groups.Size = new System.Drawing.Size(99, 23);
            this.View_Groups.TabIndex = 1;
            this.View_Groups.Text = "View";
            this.View_Groups.UseVisualStyleBackColor = true;
            this.View_Groups.Click += new System.EventHandler(this.View_Groups_Click);
            // 
            // groups_list
            // 
            this.groups_list.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groups_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.groups_list.Location = new System.Drawing.Point(0, 32);
            this.groups_list.Name = "groups_list";
            this.groups_list.ReadOnly = true;
            this.groups_list.Size = new System.Drawing.Size(541, 299);
            this.groups_list.TabIndex = 0;
            // 
            // panel14
            // 
            this.panel14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel14.BackColor = System.Drawing.Color.White;
            this.panel14.Controls.Add(this.label30);
            this.panel14.Controls.Add(this.UpdateGroup);
            this.panel14.Controls.Add(this.create_Group);
            this.panel14.Location = new System.Drawing.Point(68, 31);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(501, 112);
            this.panel14.TabIndex = 1;
            this.panel14.Paint += new System.Windows.Forms.PaintEventHandler(this.panel14_Paint_1);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label30.Location = new System.Drawing.Point(163, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(164, 25);
            this.label30.TabIndex = 3;
            this.label30.Text = "Manage Groups";
            // 
            // UpdateGroup
            // 
            this.UpdateGroup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UpdateGroup.BackColor = System.Drawing.Color.SandyBrown;
            this.UpdateGroup.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UpdateGroup.Location = new System.Drawing.Point(270, 80);
            this.UpdateGroup.Name = "UpdateGroup";
            this.UpdateGroup.Size = new System.Drawing.Size(88, 23);
            this.UpdateGroup.TabIndex = 2;
            this.UpdateGroup.Text = "Update Group";
            this.UpdateGroup.UseVisualStyleBackColor = false;
            this.UpdateGroup.Click += new System.EventHandler(this.button5_Click);
            // 
            // create_Group
            // 
            this.create_Group.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.create_Group.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.create_Group.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.create_Group.Location = new System.Drawing.Point(117, 80);
            this.create_Group.Name = "create_Group";
            this.create_Group.Size = new System.Drawing.Size(88, 23);
            this.create_Group.TabIndex = 0;
            this.create_Group.Text = "Create Group";
            this.create_Group.UseVisualStyleBackColor = false;
            this.create_Group.Click += new System.EventHandler(this.create_Group_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage4.Controls.Add(this.panel18);
            this.tabPage4.Controls.Add(this.panel16);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(610, 491);
            this.tabPage4.TabIndex = 7;
            this.tabPage4.Text = "Student Edit Group";
            // 
            // panel18
            // 
            this.panel18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel18.BackColor = System.Drawing.Color.White;
            this.panel18.Controls.Add(this.Back);
            this.panel18.Controls.Add(this.SpecificGroupStu);
            this.panel18.Location = new System.Drawing.Point(36, 226);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(565, 235);
            this.panel18.TabIndex = 1;
            // 
            // SpecificGroupStu
            // 
            this.SpecificGroupStu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SpecificGroupStu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SpecificGroupStu.Location = new System.Drawing.Point(3, 3);
            this.SpecificGroupStu.Name = "SpecificGroupStu";
            this.SpecificGroupStu.ReadOnly = true;
            this.SpecificGroupStu.Size = new System.Drawing.Size(559, 186);
            this.SpecificGroupStu.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel16.BackColor = System.Drawing.Color.Snow;
            this.panel16.Controls.Add(this.AddNewStu);
            this.panel16.Controls.Add(this.label33);
            this.panel16.Controls.Add(this.label32);
            this.panel16.Controls.Add(this.label31);
            this.panel16.Controls.Add(this.UpdateAssignedPro);
            this.panel16.Controls.Add(this.DelGroupStu);
            this.panel16.Controls.Add(this.Add_Group_Stu);
            this.panel16.Controls.Add(this.Assign_Project);
            this.panel16.Controls.Add(this.label22);
            this.panel16.Controls.Add(this.label21);
            this.panel16.Controls.Add(this.Student_list);
            this.panel16.Controls.Add(this.projects_list);
            this.panel16.Location = new System.Drawing.Point(36, 33);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(565, 172);
            this.panel16.TabIndex = 0;
            // 
            // AddNewStu
            // 
            this.AddNewStu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.AddNewStu.Location = new System.Drawing.Point(463, 132);
            this.AddNewStu.Name = "AddNewStu";
            this.AddNewStu.Size = new System.Drawing.Size(75, 23);
            this.AddNewStu.TabIndex = 14;
            this.AddNewStu.Text = "Add";
            this.AddNewStu.UseVisualStyleBackColor = false;
            this.AddNewStu.Click += new System.EventHandler(this.AddNewStu_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label33.Location = new System.Drawing.Point(346, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(111, 16);
            this.label33.TabIndex = 13;
            this.label33.Text = "Add new Student:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label32.Location = new System.Drawing.Point(7, 144);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(236, 16);
            this.label32.TabIndex = 12;
            this.label32.Text = "Update Assigned Project to the Group:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Maroon;
            this.label31.Location = new System.Drawing.Point(7, 118);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(224, 16);
            this.label31.TabIndex = 11;
            this.label31.Text = "Remove   Student  From  the   Group:";
            // 
            // UpdateAssignedPro
            // 
            this.UpdateAssignedPro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UpdateAssignedPro.BackColor = System.Drawing.Color.LightGreen;
            this.UpdateAssignedPro.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.UpdateAssignedPro.Location = new System.Drawing.Point(241, 145);
            this.UpdateAssignedPro.Name = "UpdateAssignedPro";
            this.UpdateAssignedPro.Size = new System.Drawing.Size(75, 23);
            this.UpdateAssignedPro.TabIndex = 10;
            this.UpdateAssignedPro.Text = "Update";
            this.UpdateAssignedPro.UseVisualStyleBackColor = false;
            this.UpdateAssignedPro.Click += new System.EventHandler(this.UpdateAssignedPro_Click);
            // 
            // DelGroupStu
            // 
            this.DelGroupStu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DelGroupStu.BackColor = System.Drawing.Color.Firebrick;
            this.DelGroupStu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DelGroupStu.Location = new System.Drawing.Point(241, 119);
            this.DelGroupStu.Name = "DelGroupStu";
            this.DelGroupStu.Size = new System.Drawing.Size(75, 23);
            this.DelGroupStu.TabIndex = 9;
            this.DelGroupStu.Text = "Delete";
            this.DelGroupStu.UseVisualStyleBackColor = false;
            this.DelGroupStu.Click += new System.EventHandler(this.DelGroupStu_Click);
            // 
            // Add_Group_Stu
            // 
            this.Add_Group_Stu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Add_Group_Stu.Location = new System.Drawing.Point(477, 79);
            this.Add_Group_Stu.Name = "Add_Group_Stu";
            this.Add_Group_Stu.Size = new System.Drawing.Size(75, 23);
            this.Add_Group_Stu.TabIndex = 8;
            this.Add_Group_Stu.Text = "Add";
            this.Add_Group_Stu.UseVisualStyleBackColor = false;
            this.Add_Group_Stu.Click += new System.EventHandler(this.Add_Group_Stu_Click);
            // 
            // Assign_Project
            // 
            this.Assign_Project.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Assign_Project.Location = new System.Drawing.Point(477, 33);
            this.Assign_Project.Name = "Assign_Project";
            this.Assign_Project.Size = new System.Drawing.Size(75, 23);
            this.Assign_Project.TabIndex = 7;
            this.Assign_Project.Text = "Assign";
            this.Assign_Project.UseVisualStyleBackColor = false;
            this.Assign_Project.Click += new System.EventHandler(this.Assign_Project_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(3, 79);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(228, 19);
            this.label22.TabIndex = 5;
            this.label22.Text = "Select The Group Student to add:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(3, 35);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(232, 19);
            this.label21.TabIndex = 4;
            this.label21.Text = "Select The project to be assigned:";
            // 
            // Student_list
            // 
            this.Student_list.FormattingEnabled = true;
            this.Student_list.Location = new System.Drawing.Point(241, 79);
            this.Student_list.Name = "Student_list";
            this.Student_list.Size = new System.Drawing.Size(230, 21);
            this.Student_list.TabIndex = 1;
            // 
            // projects_list
            // 
            this.projects_list.FormattingEnabled = true;
            this.projects_list.Location = new System.Drawing.Point(241, 33);
            this.projects_list.Name = "projects_list";
            this.projects_list.Size = new System.Drawing.Size(230, 21);
            this.projects_list.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage5.Controls.Add(this.panel17);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(610, 491);
            this.tabPage5.TabIndex = 8;
            this.tabPage5.Text = "Assign Avisors";
            // 
            // panel17
            // 
            this.panel17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel17.BackColor = System.Drawing.Color.White;
            this.panel17.Controls.Add(this.label41);
            this.panel17.Controls.Add(this.Load_All_Projects);
            this.panel17.Controls.Add(this.Load_Distinct_Pro);
            this.panel17.Controls.Add(this.label34);
            this.panel17.Controls.Add(this.Add_adv);
            this.panel17.Controls.Add(this.Add_co_adv);
            this.panel17.Controls.Add(this.label35);
            this.panel17.Controls.Add(this.Add_Ind_adv);
            this.panel17.Controls.Add(this.adv_projects);
            this.panel17.Controls.Add(this.adv_list);
            this.panel17.Controls.Add(this.label27);
            this.panel17.Controls.Add(this.co_adv_list);
            this.panel17.Controls.Add(this.ind_adv_list);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Controls.Add(this.label25);
            this.panel17.Controls.Add(this.label24);
            this.panel17.Location = new System.Drawing.Point(50, 31);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(515, 368);
            this.panel17.TabIndex = 4;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Tai Le", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(187, 25);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(149, 23);
            this.label41.TabIndex = 9;
            this.label41.Text = "Assign Advisors";
            // 
            // Load_All_Projects
            // 
            this.Load_All_Projects.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Load_All_Projects.BackColor = System.Drawing.Color.LightGreen;
            this.Load_All_Projects.Location = new System.Drawing.Point(395, 291);
            this.Load_All_Projects.Name = "Load_All_Projects";
            this.Load_All_Projects.Size = new System.Drawing.Size(75, 23);
            this.Load_All_Projects.TabIndex = 7;
            this.Load_All_Projects.Text = "Load";
            this.Load_All_Projects.UseVisualStyleBackColor = false;
            this.Load_All_Projects.Click += new System.EventHandler(this.Load_All_Projects_Click);
            // 
            // Load_Distinct_Pro
            // 
            this.Load_Distinct_Pro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Load_Distinct_Pro.BackColor = System.Drawing.Color.LightGreen;
            this.Load_Distinct_Pro.Location = new System.Drawing.Point(395, 317);
            this.Load_Distinct_Pro.Name = "Load_Distinct_Pro";
            this.Load_Distinct_Pro.Size = new System.Drawing.Size(75, 23);
            this.Load_Distinct_Pro.TabIndex = 8;
            this.Load_Distinct_Pro.Text = "Load";
            this.Load_Distinct_Pro.UseVisualStyleBackColor = false;
            this.Load_Distinct_Pro.Click += new System.EventHandler(this.Load_Distinct_Pro_Click);
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label34.Location = new System.Drawing.Point(51, 295);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(332, 16);
            this.label34.TabIndex = 5;
            this.label34.Text = "                                                       Load All Projects :";
            this.label34.Click += new System.EventHandler(this.label34_Click);
            // 
            // Add_adv
            // 
            this.Add_adv.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.Add_adv.BackColor = System.Drawing.Color.CadetBlue;
            this.Add_adv.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Add_adv.Location = new System.Drawing.Point(413, 146);
            this.Add_adv.Name = "Add_adv";
            this.Add_adv.Size = new System.Drawing.Size(75, 23);
            this.Add_adv.TabIndex = 6;
            this.Add_adv.Text = "Add";
            this.Add_adv.UseVisualStyleBackColor = false;
            this.Add_adv.Click += new System.EventHandler(this.Add_adv_Click);
            // 
            // Add_co_adv
            // 
            this.Add_co_adv.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.Add_co_adv.BackColor = System.Drawing.Color.CadetBlue;
            this.Add_co_adv.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Add_co_adv.Location = new System.Drawing.Point(413, 201);
            this.Add_co_adv.Name = "Add_co_adv";
            this.Add_co_adv.Size = new System.Drawing.Size(75, 23);
            this.Add_co_adv.TabIndex = 5;
            this.Add_co_adv.Text = "Add";
            this.Add_co_adv.UseVisualStyleBackColor = false;
            this.Add_co_adv.Click += new System.EventHandler(this.Add_co_adv_Click);
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label35.Location = new System.Drawing.Point(49, 321);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(334, 16);
            this.label35.TabIndex = 6;
            this.label35.Text = "Load Only Those, who are not assigned any advisor yet:";
            // 
            // Add_Ind_adv
            // 
            this.Add_Ind_adv.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.Add_Ind_adv.BackColor = System.Drawing.Color.CadetBlue;
            this.Add_Ind_adv.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Add_Ind_adv.Location = new System.Drawing.Point(413, 256);
            this.Add_Ind_adv.Name = "Add_Ind_adv";
            this.Add_Ind_adv.Size = new System.Drawing.Size(75, 23);
            this.Add_Ind_adv.TabIndex = 4;
            this.Add_Ind_adv.Text = "Add";
            this.Add_Ind_adv.UseVisualStyleBackColor = false;
            this.Add_Ind_adv.Click += new System.EventHandler(this.Add_Ind_adv_Click);
            // 
            // adv_projects
            // 
            this.adv_projects.FormattingEnabled = true;
            this.adv_projects.Location = new System.Drawing.Point(228, 79);
            this.adv_projects.Name = "adv_projects";
            this.adv_projects.Size = new System.Drawing.Size(210, 21);
            this.adv_projects.TabIndex = 0;
            // 
            // adv_list
            // 
            this.adv_list.FormattingEnabled = true;
            this.adv_list.Location = new System.Drawing.Point(228, 116);
            this.adv_list.Name = "adv_list";
            this.adv_list.Size = new System.Drawing.Size(210, 21);
            this.adv_list.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.DarkCyan;
            this.label27.Location = new System.Drawing.Point(50, 228);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(172, 21);
            this.label27.TabIndex = 3;
            this.label27.Text = "Select Industry Advisor:";
            // 
            // co_adv_list
            // 
            this.co_adv_list.FormattingEnabled = true;
            this.co_adv_list.Location = new System.Drawing.Point(228, 174);
            this.co_adv_list.Name = "co_adv_list";
            this.co_adv_list.Size = new System.Drawing.Size(210, 21);
            this.co_adv_list.TabIndex = 1;
            // 
            // ind_adv_list
            // 
            this.ind_adv_list.FormattingEnabled = true;
            this.ind_adv_list.Location = new System.Drawing.Point(228, 229);
            this.ind_adv_list.Name = "ind_adv_list";
            this.ind_adv_list.Size = new System.Drawing.Size(210, 21);
            this.ind_adv_list.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.DarkCyan;
            this.label26.Location = new System.Drawing.Point(50, 173);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(134, 21);
            this.label26.TabIndex = 2;
            this.label26.Text = "Select Co Advisor:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.DarkCyan;
            this.label25.Location = new System.Drawing.Point(50, 125);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(111, 21);
            this.label25.TabIndex = 1;
            this.label25.Text = "Select Advisor:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.DarkCyan;
            this.label24.Location = new System.Drawing.Point(50, 78);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 21);
            this.label24.TabIndex = 0;
            this.label24.Text = "Select Project:";
            // 
            // Evaluate_Group
            // 
            this.Evaluate_Group.BackColor = System.Drawing.Color.MintCream;
            this.Evaluate_Group.Controls.Add(this.dataGridView5);
            this.Evaluate_Group.Controls.Add(this.panel19);
            this.Evaluate_Group.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Evaluate_Group.Location = new System.Drawing.Point(4, 22);
            this.Evaluate_Group.Name = "Evaluate_Group";
            this.Evaluate_Group.Padding = new System.Windows.Forms.Padding(3);
            this.Evaluate_Group.Size = new System.Drawing.Size(610, 491);
            this.Evaluate_Group.TabIndex = 9;
            this.Evaluate_Group.Text = "Evaluate_Group";
            // 
            // dataGridView5
            // 
            this.dataGridView5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(78, 265);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(487, 196);
            this.dataGridView5.TabIndex = 1;
            // 
            // panel19
            // 
            this.panel19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel19.BackColor = System.Drawing.Color.Snow;
            this.panel19.Controls.Add(this.Save_upd_evl);
            this.panel19.Controls.Add(this.upd_evl_grp);
            this.panel19.Controls.Add(this.add_evaluate_group);
            this.panel19.Controls.Add(this.evaluations_list);
            this.panel19.Controls.Add(this.Total_marks_label);
            this.panel19.Controls.Add(this.label39);
            this.panel19.Controls.Add(this.label38);
            this.panel19.Controls.Add(this.label37);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Controls.Add(this.Obtained_Marks);
            this.panel19.Controls.Add(this.group_ids_list);
            this.panel19.Location = new System.Drawing.Point(78, 38);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(487, 190);
            this.panel19.TabIndex = 0;
            // 
            // Save_upd_evl
            // 
            this.Save_upd_evl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Save_upd_evl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Save_upd_evl.Location = new System.Drawing.Point(389, 165);
            this.Save_upd_evl.Name = "Save_upd_evl";
            this.Save_upd_evl.Size = new System.Drawing.Size(75, 23);
            this.Save_upd_evl.TabIndex = 10;
            this.Save_upd_evl.Text = "Save";
            this.Save_upd_evl.UseVisualStyleBackColor = false;
            this.Save_upd_evl.Click += new System.EventHandler(this.Save_upd_evl_Click);
            // 
            // upd_evl_grp
            // 
            this.upd_evl_grp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.upd_evl_grp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.upd_evl_grp.Location = new System.Drawing.Point(308, 165);
            this.upd_evl_grp.Name = "upd_evl_grp";
            this.upd_evl_grp.Size = new System.Drawing.Size(75, 23);
            this.upd_evl_grp.TabIndex = 9;
            this.upd_evl_grp.Text = "Update";
            this.upd_evl_grp.UseVisualStyleBackColor = false;
            this.upd_evl_grp.Click += new System.EventHandler(this.upd_evl_grp_Click);
            // 
            // add_evaluate_group
            // 
            this.add_evaluate_group.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.add_evaluate_group.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.add_evaluate_group.Location = new System.Drawing.Point(227, 165);
            this.add_evaluate_group.Name = "add_evaluate_group";
            this.add_evaluate_group.Size = new System.Drawing.Size(75, 23);
            this.add_evaluate_group.TabIndex = 8;
            this.add_evaluate_group.Text = "ADD";
            this.add_evaluate_group.UseVisualStyleBackColor = false;
            this.add_evaluate_group.Click += new System.EventHandler(this.add_evaluate_group_Click);
            // 
            // evaluations_list
            // 
            this.evaluations_list.FormattingEnabled = true;
            this.evaluations_list.Location = new System.Drawing.Point(142, 31);
            this.evaluations_list.Name = "evaluations_list";
            this.evaluations_list.Size = new System.Drawing.Size(258, 21);
            this.evaluations_list.TabIndex = 1;
            this.evaluations_list.SelectedIndexChanged += new System.EventHandler(this.evaluations_list_SelectedIndexChanged);
            // 
            // Total_marks_label
            // 
            this.Total_marks_label.AutoSize = true;
            this.Total_marks_label.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Total_marks_label.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Total_marks_label.Location = new System.Drawing.Point(142, 112);
            this.Total_marks_label.Name = "Total_marks_label";
            this.Total_marks_label.Size = new System.Drawing.Size(0, 16);
            this.Total_marks_label.TabIndex = 7;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label39.Location = new System.Drawing.Point(18, 105);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 16);
            this.label39.TabIndex = 6;
            this.label39.Text = "Total Marks:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label38.Location = new System.Drawing.Point(18, 142);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(105, 16);
            this.label38.TabIndex = 5;
            this.label38.Text = "Obtained Marks:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label37.Location = new System.Drawing.Point(18, 36);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(108, 16);
            this.label37.TabIndex = 4;
            this.label37.Text = "Select Evaluation:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label36.Location = new System.Drawing.Point(18, 68);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(101, 16);
            this.label36.TabIndex = 3;
            this.label36.Text = "Select Group Id:";
            // 
            // Obtained_Marks
            // 
            this.Obtained_Marks.Location = new System.Drawing.Point(142, 138);
            this.Obtained_Marks.Name = "Obtained_Marks";
            this.Obtained_Marks.Size = new System.Drawing.Size(258, 20);
            this.Obtained_Marks.TabIndex = 2;
            // 
            // group_ids_list
            // 
            this.group_ids_list.FormattingEnabled = true;
            this.group_ids_list.Location = new System.Drawing.Point(145, 68);
            this.group_ids_list.Name = "group_ids_list";
            this.group_ids_list.Size = new System.Drawing.Size(258, 21);
            this.group_ids_list.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Azure;
            this.tabPage6.Controls.Add(this.panel20);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(610, 491);
            this.tabPage6.TabIndex = 10;
            this.tabPage6.Text = "Reports";
            // 
            // panel20
            // 
            this.panel20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel20.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel20.Controls.Add(this.label45);
            this.panel20.Controls.Add(this.ProjectAllocationReport);
            this.panel20.Controls.Add(this.label44);
            this.panel20.Controls.Add(this.student_who_changed_group);
            this.panel20.Controls.Add(this.label43);
            this.panel20.Controls.Add(this.top10groups);
            this.panel20.Controls.Add(this.label42);
            this.panel20.Controls.Add(this.Generate_Mark_Sheet);
            this.panel20.Controls.Add(this.advisory_board_report);
            this.panel20.Controls.Add(this.label40);
            this.panel20.Location = new System.Drawing.Point(64, 55);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(496, 373);
            this.panel20.TabIndex = 0;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(16, 312);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(286, 21);
            this.label45.TabIndex = 6;
            this.label45.Text = "Project Allocation Over view Report.";
            // 
            // ProjectAllocationReport
            // 
            this.ProjectAllocationReport.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ProjectAllocationReport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ProjectAllocationReport.Location = new System.Drawing.Point(407, 312);
            this.ProjectAllocationReport.Name = "ProjectAllocationReport";
            this.ProjectAllocationReport.Size = new System.Drawing.Size(75, 28);
            this.ProjectAllocationReport.TabIndex = 5;
            this.ProjectAllocationReport.Text = "Generate";
            this.ProjectAllocationReport.UseVisualStyleBackColor = false;
            this.ProjectAllocationReport.Click += new System.EventHandler(this.ProjectAllocationReport_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(16, 237);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(388, 42);
            this.label44.TabIndex = 5;
            this.label44.Text = " Report to determine students who changed their \r\ngroup.";
            // 
            // student_who_changed_group
            // 
            this.student_who_changed_group.BackColor = System.Drawing.Color.DarkSlateGray;
            this.student_who_changed_group.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.student_who_changed_group.Location = new System.Drawing.Point(407, 233);
            this.student_who_changed_group.Name = "student_who_changed_group";
            this.student_who_changed_group.Size = new System.Drawing.Size(75, 28);
            this.student_who_changed_group.TabIndex = 4;
            this.student_who_changed_group.Text = "Generate";
            this.student_who_changed_group.UseVisualStyleBackColor = false;
            this.student_who_changed_group.Click += new System.EventHandler(this.student_who_changed_group_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(16, 166);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(301, 21);
            this.label43.TabIndex = 4;
            this.label43.Text = " Report on determining top 10 groups.";
            // 
            // top10groups
            // 
            this.top10groups.BackColor = System.Drawing.Color.DarkSlateGray;
            this.top10groups.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.top10groups.Location = new System.Drawing.Point(407, 166);
            this.top10groups.Name = "top10groups";
            this.top10groups.Size = new System.Drawing.Size(75, 28);
            this.top10groups.TabIndex = 3;
            this.top10groups.Text = "Generate";
            this.top10groups.UseVisualStyleBackColor = false;
            this.top10groups.Click += new System.EventHandler(this.top10groups_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(12, 87);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(389, 42);
            this.label42.TabIndex = 3;
            this.label42.Text = " Marks sheet of projects that shows the marks in\r\n each evaluation against each s" +
    "tudent and project";
            // 
            // Generate_Mark_Sheet
            // 
            this.Generate_Mark_Sheet.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Generate_Mark_Sheet.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Generate_Mark_Sheet.Location = new System.Drawing.Point(407, 101);
            this.Generate_Mark_Sheet.Name = "Generate_Mark_Sheet";
            this.Generate_Mark_Sheet.Size = new System.Drawing.Size(75, 28);
            this.Generate_Mark_Sheet.TabIndex = 2;
            this.Generate_Mark_Sheet.Text = "Generate";
            this.Generate_Mark_Sheet.UseVisualStyleBackColor = false;
            this.Generate_Mark_Sheet.Click += new System.EventHandler(this.Generate_Mark_Sheet_Click);
            // 
            // advisory_board_report
            // 
            this.advisory_board_report.BackColor = System.Drawing.Color.DarkSlateGray;
            this.advisory_board_report.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advisory_board_report.Location = new System.Drawing.Point(407, 35);
            this.advisory_board_report.Name = "advisory_board_report";
            this.advisory_board_report.Size = new System.Drawing.Size(75, 28);
            this.advisory_board_report.TabIndex = 1;
            this.advisory_board_report.Text = "Generate";
            this.advisory_board_report.UseVisualStyleBackColor = false;
            this.advisory_board_report.Click += new System.EventHandler(this.advisory_board_report_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(16, 21);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(348, 42);
            this.label40.TabIndex = 0;
            this.label40.Text = "Report of list of projects along with advisory\r\n board and list of students.";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.Controls.Add(this.tabControl1);
            this.panel5.Location = new System.Drawing.Point(182, 186);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(618, 493);
            this.panel5.TabIndex = 2;
            // 
            // Back
            // 
            this.Back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Back.Location = new System.Drawing.Point(477, 209);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(75, 23);
            this.Back.TabIndex = 1;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // Add_Persons
            // 
            this.Add_Persons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Add_Persons.BackColor = System.Drawing.Color.Transparent;
            this.Add_Persons.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Add_Persons.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_Persons.ForeColor = System.Drawing.Color.Teal;
            this.Add_Persons.Image = global::Mid_Project.Properties.Resources.assign_advisor;
            this.Add_Persons.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Add_Persons.Location = new System.Drawing.Point(19, 53);
            this.Add_Persons.Name = "Add_Persons";
            this.Add_Persons.Size = new System.Drawing.Size(140, 26);
            this.Add_Persons.TabIndex = 0;
            this.Add_Persons.Text = "Register";
            this.Add_Persons.UseVisualStyleBackColor = false;
            this.Add_Persons.Click += new System.EventHandler(this.Add_Persons_Click);
            this.Add_Persons.Enter += new System.EventHandler(this.Add_Persons_Enter);
            this.Add_Persons.Leave += new System.EventHandler(this.Add_Persons_Leave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 679);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(500, 600);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.PersonCrud.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.Welcome_Page.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.StudendCrud.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.Project_Crud.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groups_list)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SpecificGroupStu)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.Evaluate_Group.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage PersonCrud;
        private System.Windows.Forms.TabPage Welcome_Page;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Add_Person;
        private System.Windows.Forms.RadioButton is_Advisor;
        private System.Windows.Forms.TextBox P_Email;
        private System.Windows.Forms.TextBox P_Contact;
        private System.Windows.Forms.TextBox LastName;
        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Contact;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage StudendCrud;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button Search_Stu;
        private System.Windows.Forms.Button Delete_Stu;
        private System.Windows.Forms.Button Register_Student;
        private System.Windows.Forms.TextBox Registration_No;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button update_Stu;
        private System.Windows.Forms.Button update_Student;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.RadioButton g_female;
        private System.Windows.Forms.RadioButton g_male;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button update_advisor;
        private System.Windows.Forms.Button upd_advisor;
        private System.Windows.Forms.Button Search_Advisor;
        private System.Windows.Forms.Button Delete_Advisor;
        private System.Windows.Forms.Button Register_advisor;
        private System.Windows.Forms.TextBox A_Salary;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ComboBox designations;
        private System.Windows.Forms.Button View_STU_button;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage Project_Crud;
        private System.Windows.Forms.Button View_Advisors_Button;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Advisor_Search_TextBox;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button Delete_Project;
        private System.Windows.Forms.Button Search_Project;
        private System.Windows.Forms.Button update_Project;
        private System.Windows.Forms.Button Add_Project;
        private System.Windows.Forms.TextBox p_description;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox p_Title;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton is_Student;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button View_Projects;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button Delete_Eval;
        private System.Windows.Forms.Button Search_Eval;
        private System.Windows.Forms.Button Upd_Eval;
        private System.Windows.Forms.TextBox E_Weightage;
        private System.Windows.Forms.TextBox E_Marks;
        private System.Windows.Forms.TextBox E_Name;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button Add_Eval;
        private System.Windows.Forms.Button View_Eval;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button Manage_Reports;
        private System.Windows.Forms.Button Conduct_Evaluations;
        private System.Windows.Forms.Button Assign_Advisor;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button crud_advisor;
        private System.Windows.Forms.Button Student_CRUD;
        private System.Windows.Forms.Button Add_Persons;
        private System.Windows.Forms.Button Evaluations_Crud;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button View_Groups;
        private System.Windows.Forms.DataGridView groups_list;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button create_Group;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.ComboBox Student_list;
        private System.Windows.Forms.ComboBox projects_list;
        private System.Windows.Forms.Button UpdateGroup;
        private System.Windows.Forms.Button Add_Group_Stu;
        private System.Windows.Forms.Button Assign_Project;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button Add_adv;
        private System.Windows.Forms.Button Add_co_adv;
        private System.Windows.Forms.Button Add_Ind_adv;
        private System.Windows.Forms.ComboBox adv_projects;
        private System.Windows.Forms.ComboBox adv_list;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox co_adv_list;
        private System.Windows.Forms.ComboBox ind_adv_list;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.DataGridView SpecificGroupStu;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button UpdateAssignedPro;
        private System.Windows.Forms.Button DelGroupStu;
        private System.Windows.Forms.Button AddNewStu;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button Load_Distinct_Pro;
        private System.Windows.Forms.Button Load_All_Projects;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage Evaluate_Group;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox Obtained_Marks;
        private System.Windows.Forms.ComboBox evaluations_list;
        private System.Windows.Forms.ComboBox group_ids_list;
        private System.Windows.Forms.Button upd_evl_grp;
        private System.Windows.Forms.Button add_evaluate_group;
        private System.Windows.Forms.Label Total_marks_label;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button Save_upd_evl;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button Generate_Mark_Sheet;
        private System.Windows.Forms.Button top10groups;
        private System.Windows.Forms.Button student_who_changed_group;
        private System.Windows.Forms.Button ProjectAllocationReport;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button advisory_board_report;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button Back;
    }
}

