﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public class Advisor : Person
    {
        public int Designation { get; set; }
        public decimal Salary { get; set; }

        public Advisor(string firstName, string lastName, string contact, string email, DateTime dateOfBirth, int gender, int designation, decimal salary)
            : base(firstName, lastName, contact, email, dateOfBirth, gender)
        {
            Designation = designation;
            Salary = salary;
        }

        public static string AddAdvisor(Advisor advisor)
        {
            var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) " +
                               "VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender);" +
                               "INSERT INTO Advisor (Id, Designation, Salary) VALUES (SCOPE_IDENTITY(), @Designation, @Salary);";
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@FirstName", advisor.FirstName);
                command.Parameters.AddWithValue("@LastName", advisor.LastName);
                command.Parameters.AddWithValue("@Contact", advisor.Contact);
                command.Parameters.AddWithValue("@Email", advisor.Email);
                command.Parameters.AddWithValue("@DateOfBirth", advisor.DateOfBirth);
                command.Parameters.AddWithValue("@Gender", advisor.Gender);
                command.Parameters.AddWithValue("@Designation", advisor.Designation);
                command.Parameters.AddWithValue("@Salary", advisor.Salary);
                command.ExecuteNonQuery();
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        // Helper method to check if the designation exists 
        public static bool IsDesignationValid(int designation)
        {
            var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "SELECT COUNT(*) FROM Lookup WHERE Id = @Designation AND Category = 'Designation'";
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@Designation", designation);

                int count = (int)command.ExecuteScalar();

                return count > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false;
            }

        }

        public static string delete_advisor(int advisorId)
        {
            var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "DELETE FROM Advisor WHERE Id = @AdvisorId";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@AdvisorId", advisorId);

                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected > 0)
                {
                    return("Advisor deleted successfully.");
                }
                else
                {
                   return("Failed to delete advisor.");
                }
            }
            catch(Exception ex)
            {
                return (ex.Message);
            }
        }

        public static bool MarkAdvisorDeleted(int advisorId)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Person SET FirstName = '*' + FirstName " +
                                   "FROM Advisor " +
                                   "INNER JOIN Person ON Advisor.Id = Person.Id " +
                                   "WHERE Advisor.Id = @AdvisorId";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@AdvisorId", advisorId);

                    int rowsAffected = command.ExecuteNonQuery();

                    return rowsAffected > 0; // Return true if one or more rows were affected (updated)

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false; // Update failed
            }
        }
        
        public static string UpdateAdvisor(int advisorId, Advisor a)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string advisorQuery = "UPDATE Advisor SET Designation = @Designation, Salary = @Salary WHERE Id = @AdvisorId";
                string personQuery = "UPDATE Person SET FirstName = @FirstName, LastName = @LastName, " +
                                     "Contact = @Contact, Email = @Email, DateOfBirth = @DateOfBirth, " +
                                     "Gender = @Gender " +
                                     "WHERE Id = @AdvisorId";

                SqlCommand advisorCmd = new SqlCommand(advisorQuery, con);
                advisorCmd.Parameters.AddWithValue("@Salary", a.Salary);
                advisorCmd.Parameters.AddWithValue("@Designation", a.Designation);
                advisorCmd.Parameters.AddWithValue("@AdvisorId", advisorId);

                SqlCommand personCmd = new SqlCommand(personQuery, con);
                personCmd.Parameters.AddWithValue("@FirstName", a.FirstName);
                personCmd.Parameters.AddWithValue("@LastName", a.LastName);
                personCmd.Parameters.AddWithValue("@Contact", a.Contact);
                personCmd.Parameters.AddWithValue("@Email", a.Email);
                personCmd.Parameters.AddWithValue("@DateOfBirth", a.DateOfBirth);
                personCmd.Parameters.AddWithValue("@Gender", a.Gender);
                personCmd.Parameters.AddWithValue("@AdvisorId", advisorId);

                int advisorRowsAffected = advisorCmd.ExecuteNonQuery();

                int personRowsAffected = personCmd.ExecuteNonQuery();


                // Check if both updates were successful
                if (advisorRowsAffected > 0 && personRowsAffected > 0)
                {
                    return null; 
                }
                else
                {
                    return "Error updating Advisor and Person records."; // Error
                }
            }
            catch (Exception ex)
            {
                return ex.Message; // Exception occurred
            }
        }


        public static DataTable AdvisorSearchTable(string searchTerm)
        {
            DataTable dt = new DataTable();

            try
            {
                var con = Configuration.getInstance().getConnection();
                    string query = "SELECT Advisor.Id, Person.FirstName, Person.LastName, " +
                                   "Person.Contact, Person.Email, Person.DateOfBirth, Person.Gender, " +
                                   "Advisor.Designation, Advisor.Salary " +
                                   "FROM Advisor " +
                                   "INNER JOIN Person ON Advisor.Id = Person.Id " +
                                   "WHERE Person.FirstName LIKE @SearchTerm  OR " +
                                   "Person.LastName LIKE @SearchTerm OR " +
                                   "Person.Contact LIKE @SearchTerm OR " +
                                   "Person.Email LIKE @SearchTerm And Person.FirstName Not Like '*%'  ";

                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@SearchTerm", "%" + searchTerm + "%");

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show("Failed to search for advisors. Please try again.");
            }

            return dt;
        }
        public static Advisor GetAdvisorById(int advisorId)
        {
            Advisor advisor = null;

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Advisor.Id, Person.FirstName, Person.LastName, " +
                               "Person.Contact, Person.Email, Person.DateOfBirth, Person.Gender, " +
                               "Advisor.Designation, Advisor.Salary " +
                               "FROM Advisor " +
                               "INNER JOIN Person ON Advisor.Id = Person.Id " +
                               "WHERE Advisor.Id = @AdvisorId";

                var cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@AdvisorId", advisorId);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    string firstName = reader["FirstName"].ToString();
                    string lastName = reader["LastName"].ToString();
                    string contact = reader["Contact"].ToString();
                    string email = reader["Email"].ToString();
                    DateTime dateOfBirth = Convert.ToDateTime(reader["DateOfBirth"]);
                    int gender = Convert.ToInt32(reader["Gender"]);
                    int designation = Convert.ToInt32(reader["Designation"]);
                    decimal salary = Convert.ToDecimal(reader["Salary"]);

                    advisor = new Advisor(firstName, lastName, contact, email, dateOfBirth, gender, designation, salary);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return advisor;
        }
    }

        
}
