﻿using iText.Layout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class FYP_Manager : Form
    {
        Person p;
        string update_reg;
        int upd_stu_id;
        int upd_advisorId;
        int upd_desig;
        int upd_group_id;
        decimal upd_salry;
        
        public FYP_Manager()
        {
            InitializeComponent();
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ReadOnly = true;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.ReadOnly = true;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.ReadOnly = true;
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private Person takePersonInput()
        {
            string f_name = FirstName.Text;
            if (!Validator.valid_name(f_name))
            {
                MessageBox.Show("First name is not valid.");
                return null;
            }
            string l_name = LastName.Text;
            if (!Validator.valid_name(l_name) && !(l_name == ""))
            {
                MessageBox.Show("last name is not valid.");
                return null;
            }
            string contact = P_Contact.Text;
            if (!Validator.ValidatePhoneNumber(contact) && !(P_Contact.Text == ""))
            {
                MessageBox.Show("for valid phone number use 11 digit contact.");
                return null;
            }
            string email = P_Email.Text;
            if (!Validator.email(email))
            {
                MessageBox.Show("Enter valid email i.e. X@gmail.com");
                return null;
            }
            DateTime dob = dateTimePicker1.Value;
            if(!Validator.ValidateDOB(dob))
            {
                MessageBox.Show("Choose valid date of birth. Age can be no more less than 18 years.");
                return null;
            }
            int gender = -1;
            if (g_male.Checked == true)
            {
                g_female.Checked = false;
                gender = 1;
            }
            else if (g_female.Checked == true)
            {
                g_male.Checked = false;
                gender = 2;
            }
            if (gender == -1)
            {
                return null;
            }
            Person p = new Person(f_name, l_name, contact, email, dob, gender);

            return p;
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Add_Person_Click(object sender, EventArgs e)
        {
            p = takePersonInput();
            if (p != null)
            {
                if (is_Student.Checked == true)
                {
                    Register_Student.Visible = true;
                    is_Advisor.Checked = false;
                    tabControl1.Visible = false;
                    tabControl1.SelectedIndex = 2;
                    tabControl1.Visible = true;
                    MessageBox.Show("Enter Registration Number and then click register, to add the student.");
                }
                else if (is_Advisor.Checked == true)
                {
                    Register_advisor.Visible = true;
                    is_Student.Checked = false;
                    tabControl1.Visible = false;
                    tabControl1.SelectedIndex = 3;
                    tabControl1.Visible = true;
                    MessageBox.Show("Enter Salary And Designationand press register to add advisor.");
                }
                else
                {
                    MessageBox.Show("Choose you want to advisor or Student.");
                }
            }

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {
            panel4.BackColor = Color.FromArgb(180, Color.Teal);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            update_Student.Visible = false;
            update_advisor.Visible = false;
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 1;
            tabControl1.Visible = true;
        }

        private void Register_Student_Click(object sender, EventArgs e)
        {
            string registration_No = Registration_No.Text.ToString();
            if (Registration_No.Text != "" && Validator.ValidateRegistrationNumber(registration_No) && !Student.RegistrationNumberExists(registration_No))
            {
                registration_No = Registration_No.Text;
            }
            else
            {
                MessageBox.Show("Enter valid Registration Number in format \n Year-Degree-RollNumber i.e. 2022-CS-112. ");
                return;
            }
            Student s = new Student(p.FirstName, p.LastName, p.Contact, p.Email, p.DateOfBirth, p.Gender, registration_No);
            string m = Student.AddStudent(s);
            if (m != "")
            {
                MessageBox.Show(m);
            }
            else
            {
                Register_Student.Visible = false;
                Registration_No.Text = "";
                MessageBox.Show("Student Added Successfully");
            }
        }

        private void update_Stu_Click(object sender, EventArgs e)
        {
            // contains functionality to traverse to student edit page
            string reg_NO = Registration_No.Text;
            if (Student.RegistrationNumberExists(reg_NO))
            {
                Student s = Student.GetStudentByRegistrationNumber(reg_NO);
                upd_stu_id = Student.GetStudentId(reg_NO);
                FirstName.Text = s.FirstName;
                LastName.Text = s.LastName;
                P_Contact.Text = s.Contact;
                P_Email.Text = s.Email;
                if (s.Gender == 1)
                {
                    g_male.Checked = true;
                }
                else
                {
                    g_female.Checked = true;
                }
                dateTimePicker1.Value = s.DateOfBirth;
                update_reg = reg_NO;
                update_Student.Visible = true;
                is_Student.Visible = false;
                is_Advisor.Visible = false;
                Add_Person.Visible = false;
                update_advisor.Visible = false;
                tabControl1.Visible = false;
                tabControl1.SelectedIndex = 0;
                tabControl1.Visible = true;

            }
            else
            {
                MessageBox.Show("Invalid Registration Number." + reg_NO);
            }

        }
        private void PersonClearForm()
        {
            // Clear all input fields in the form
            FirstName.Text = "";
            LastName.Text = "";
            P_Contact.Text = "";
            P_Email.Text = "";
            dateTimePicker1.Value = DateTime.Today;
            g_female.Checked = false;
            g_male.Checked = false;
            is_Advisor.Checked = false;
            is_Student.Checked = false;
        }
        private void update_Student_Click(object sender, EventArgs e)
        {
            // Implement actual updating student functionalities
            Person p = takePersonInput();

            Student s = new Student(p.FirstName, p.LastName, p.Contact, p.Email, p.DateOfBirth, p.Gender, update_reg);
            string msg = Student.UpdateStudent(s, upd_stu_id);
            if (msg != null)
            {
                MessageBox.Show(msg);
            }
            else
            {
                LoadAllStudents();
                MessageBox.Show("Student updated successfully.");
                configure_Person_Page();
            }
        }


        public void configure_Person_Page()
        {
            PersonClearForm();
            update_Student.Visible = false;
            update_advisor.Visible = false;
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 1;
            tabControl1.Visible = true;

        }
        private void Add_Persons_Click(object sender, EventArgs e)
        {
            // Functionalities For the Add Persons button on left pane
            update_Student.Visible = false;
            update_advisor.Visible = false;
            tabControl1.Visible = false;
            Add_Person.Visible = true;
            tabControl1.SelectedIndex = 0;
            tabControl1.Visible = true;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void crud_advisor_Click(object sender, EventArgs e)
        {
            Register_advisor.Visible = false;
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 3;
            tabControl1.Visible = true;
        }

        private void Register_advisor_Click(object sender, EventArgs e)
        {
            string selectedDesignation = designations.SelectedItem.ToString();
            int designation = GetDesignationCode(selectedDesignation);
            decimal salary = 0;

            if (!Advisor.IsDesignationValid(designation))
            {
                MessageBox.Show("Choose a valid designation.");
                return;
            }

            if (!decimal.TryParse(A_Salary.Text, out salary) || salary <= 0)
            {
                MessageBox.Show("Enter a valid positive salary.");
                return;
            }

            Advisor a = new Advisor(p.FirstName, p.LastName, p.Contact, p.Email, p.DateOfBirth, p.Gender, designation, salary);
            string message = Advisor.AddAdvisor(a);

            if (!string.IsNullOrEmpty(message))
            {
                MessageBox.Show(message);
            }
            else
            {
                Register_advisor.Visible = false;
                LoadAllAdvisors();
                MessageBox.Show("Advisor Added Successfully");
            }
        }

        private int GetDesignationCode(string designation)
        {
            switch (designation)
            {
                case "Professor":
                    return 6;
                case "Associate Professor":
                    return 7;
                case "Assistant Professor":
                    return 8;
                case "Lecturer":
                    return 9;
                case "Industry Professional":
                    return 10;
                default:
                    return 0; // Invalid designation
            }
        }


        private string GetDesignation(int designationCode)
        {
            switch (designationCode)
            {
                case 6:
                    return "Professor";
                case 7:
                    return "Associate Professor";
                case 8:
                    return "Assistant Professor";
                case 9:
                    return "Lecturer";
                case 10:
                    return "Industry Professional";
                default:
                    return "Invalid designation code";
            }
        }


        private void Student_CRUD_Click(object sender, EventArgs e)
        {
            is_Advisor.Checked = true;
            is_Student.Checked = true;
            Register_Student.Visible = false;
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 2;
            tabControl1.Visible = true;
        }

        private void Delete_Stu_Click(object sender, EventArgs e)
        {
            String reg = Registration_No.Text.ToString();
            if (!Validator.ValidateRegistrationNumber(reg) || !Student.RegistrationNumberExists(reg))
            {
                MessageBox.Show("Enter valid Registration Number.");
            }
            else
            {
                if (Student.MarkStudentDeleted(reg))
                {
                    LoadAllStudents();
                    MessageBox.Show("Student Deleted Successfully.");
                }
            }
        }
        public void LoadAllStudents()
        {
            try
            {
                SqlConnection con = Configuration.getInstance().getConnection();
                string query = "SELECT Student.RegistrationNo, Person.FirstName, " +
                               "Person.LastName, Person.Contact, Person.Email, " +
                               "Person.DateOfBirth, Person.Gender " +
                               "FROM Student " +
                               "INNER JOIN Person ON Student.Id = Person.Id " +
                               "WHERE LEFT(Person.FirstName, 1) <> '*'";

                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Console.WriteLine(ex.Message);
                MessageBox.Show("Failed to retrieve student data. Please try again.");

            }

        }

        public void SearchStudentByRegistrationNumber(string registrationNumber)
        {
            try
            {
                SqlConnection con = Configuration.getInstance().getConnection();

                string query = "SELECT Student.RegistrationNo, Person.FirstName, Person.LastName, " +
                               "Person.Contact, Person.Email, Person.DateOfBirth, Person.Gender " +
                               "FROM Student " +
                               "INNER JOIN Person ON Student.Id = Person.Id " +
                               "WHERE Student.RegistrationNo = @RegistrationNo and Person.FirstName Not Like '*%'";

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                // Check if any rows were returned
                if (dt.Rows.Count > 0)
                {
                    // Display the student information in the DataGridView
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No student found with the registration number: " + registrationNumber);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show(ex.Message);

                // Display an error message to the user if the search operation fails
                MessageBox.Show("Failed to search for student. Please try again.");
            }
        }

        private void View_STU_button_Click(object sender, EventArgs e)
        {
            LoadAllStudents();
        }

        private void Search_Stu_Click(object sender, EventArgs e)
        {
            String reg = Registration_No.Text.ToString();
            if (!Validator.ValidateRegistrationNumber(reg))
            {
                MessageBox.Show("Enter valid Registration Number.");
                return;
            }
            if (!Student.RegistrationNumberExists(reg))
            {
                MessageBox.Show("Registration Number does not exists.");
                return;
            }
            else
            {
                SearchStudentByRegistrationNumber(reg);
            }
        }

        private void g_male_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void P_Email_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }


        public void DeleteAdvisorFromDataGridView()
        {
            try
            {
                // Check if a row is selected in the DataGridView
                if (dataGridView2.SelectedRows.Count > 0)
                {
                    // Get the selected row
                    DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                    // Retrieve the advisor's ID from the selected row
                    int advisorId = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    bool msg = Advisor.MarkAdvisorDeleted(advisorId);
                    if (!msg)
                    {
                        MessageBox.Show("Deletion failed. Please try again.");
                    }
                    else
                    {
                        MessageBox.Show("Advisor Deleted Successfully.");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a row to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show("An error occurred while deleting advisor. Please try again.");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void Delete_Advisor_Click(object sender, EventArgs e)
        {
            DeleteAdvisorFromDataGridView();
            LoadAllAdvisors();
        }



        private void upd_advisor_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row

                string message = "Have you entered the updated designation and Salary.";
                String title = "Update Advisor";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, title, buttons);
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];
                if (result == DialogResult.Yes)
                {

                    // Retrieve the advisor's ID from the selected row
                    upd_advisorId = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    string selectedDesignation = designations.SelectedItem.ToString();
                    upd_desig = GetDesignationCode(selectedDesignation);
                    if (A_Salary.Text != "")
                    {
                        string msg = Validator.ValidateSalary(A_Salary.Text.ToString());
                        upd_salry = decimal.Parse(A_Salary.Text.ToString());
                    }
                    else
                    {
                        upd_salry = 0;
                    }
                    Advisor a = Advisor.GetAdvisorById(upd_advisorId);
                    string desig = GetDesignation(a.Designation);
                    designations.SelectedItem = desig;
                    A_Salary.Text = (a.Salary).ToString();
                    FirstName.Text = a.FirstName;
                    LastName.Text = a.LastName;
                    P_Contact.Text = a.Contact;
                    P_Email.Text = a.Email;
                    if (a.Gender == 1)
                    {
                        g_male.Checked = true;
                    }
                    else
                    {
                        g_female.Checked = true;
                    }
                    dateTimePicker1.Value = a.DateOfBirth;
                    update_Student.Visible = false;
                    is_Student.Visible = false;
                    is_Advisor.Visible = false;
                    Add_Person.Visible = false;
                    update_advisor.Visible = true;
                    tabControl1.Visible = false;
                    tabControl1.SelectedIndex = 0;
                    tabControl1.Visible = true;
                }

            }
            else
            {
                MessageBox.Show("Please select a row to update.");
            }
        }




        private void update_advisor_Click(object sender, EventArgs e)
        {
            Person p = takePersonInput();
            Advisor a = new Advisor(p.FirstName, p.LastName, p.Contact, p.Email, p.DateOfBirth, p.Gender, upd_desig, upd_salry);
            string msg = Advisor.UpdateAdvisor(upd_advisorId, a);
            if (msg == null)
            {
                MessageBox.Show("Advisor updated successfully");
                LoadAllAdvisors();
                PersonClearForm();
            }
            else
            {
                MessageBox.Show(msg);
            }
        }

        private void View_Advisors_Button_Click(object sender, EventArgs e)
        {
            LoadAllAdvisors();
        }

        public void LoadAllAdvisors()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Advisor.Id, Person.FirstName, Person.LastName, " +
                               "Person.Contact, Person.Email, Person.DateOfBirth, Person.Gender, " +
                               "Lookup.Value AS Designation, Advisor.Salary " +
                               "FROM Advisor " +
                               "INNER JOIN Person ON Advisor.Id = Person.Id " +
                               "INNER JOIN Lookup ON Advisor.Designation = Lookup.Id " +
                               "WHERE Lookup.Category = 'DESIGNATION' And LEFT(Person.FirstName, 1) <> '*'";

                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView2.DataSource = dt;
                dataGridView2.Columns["Id"].Visible = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show(ex.Message);
                MessageBox.Show("Failed to retrieve advisor data. Please try again.");
            }
        }

        public void DisplayAdvisorSearchResults(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                // Display search results in DataGridView
                dataGridView2.DataSource = dt;

            }
            else
            {
                MessageBox.Show("No advisor found matching the search term.");
            }
        }
        private void Search_Advisor_Click(object sender, EventArgs e)
        {
            string searchTerm = Advisor_Search_TextBox.Text.Trim();

            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                MessageBox.Show("Please enter a search term.");
            }
            else
            {
                DataTable searchResults = Advisor.AdvisorSearchTable(searchTerm);
                DisplayAdvisorSearchResults(searchResults);
            }
        }

        private void clearProjectPage()
        {
            p_Title.Text = "";
            p_description.Text = "";
        }
        private void Add_Project_Click(object sender, EventArgs e)
        {
            string title = p_Title.Text.ToString();
            if (title == "" || !Validator.isValidTitle(title) || !Project.IsTitleUnique(title))
            {
                MessageBox.Show("Enter Valid Title.");
                return;
            }
            string description = p_description.Text.ToString();
            Project p = new Project(title, description);
            string msg = Project.AddProject(p);
            MessageBox.Show(msg);
            clearProjectPage();
            LoadProjects();
        }

        private void designations_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FirstName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void A_Salary_TextChanged(object sender, EventArgs e)
        {

        }

        private void update_Project_Click(object sender, EventArgs e)
        {
            if (dataGridView3.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView3.SelectedRows[0];
                int proj_id = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                string title;
                string description;
                String header = "Update Project";
                string message = "Have you entered the updated values in the textboxes?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                
                DialogResult result = MessageBox.Show(message, header, buttons);
                if (result == DialogResult.Yes)
                {
                    title = p_Title.Text.ToString();
                    if (!Validator.isValidTitle(title))
                    {
                        MessageBox.Show("Enter Valid Title.");
                        return;
                    }
                    description = p_description.Text.ToString();
                    Project p1 = new Project(title, description);
                    string msg = Project.UpdateProject(proj_id, p1);
                    MessageBox.Show(msg);
                    p_Title.Enabled = true;
                    clearProjectPage();
                    LoadProjects();
                }
                else
                {
                    Project p = Project.GetProjectById(proj_id);
                    p_Title.Text = p.Title;
                    p_description.Text = p.Description;
                    p_Title.Enabled = false;
                }

            }
            else
            {
                MessageBox.Show("Select Row Update.");
            }
        }

        private void LoadProjects()
        {
            DataTable dt = Project.GetAllProjects();
            if (dt != null)
            {
                dataGridView3.DataSource = dt;
                dataGridView3.Columns["Id"].Visible = false;
            }

        }
        private void View_Projects_Click(object sender, EventArgs e)
        {
            LoadProjects();
        }

        private void Search_Project_Click(object sender, EventArgs e)
        {
            string title = p_Title.Text.ToString();
            if (p_Title.Text == "" || !Validator.isValidTitle(title))
            {
                MessageBox.Show("Enter Valid Title.");
                return;
            }
            SearchProjectsByTitle(title);
        }

        private void SearchProjectsByTitle(string title)
        {
            List<Project> projects = Project.SearchProjectByTitle(title);
            // Bind the search results to the DataGridView
            if (projects != null)
            {
                dataGridView3.DataSource = projects;
            }

        }

        private void Delete_Project_Click(object sender, EventArgs e)
        {
            if (dataGridView3.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView3.SelectedRows[0];
                int proj_id = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                string msg = Project.DeleteProject(proj_id);
                MessageBox.Show(msg);
                LoadProjects();
            }
            else
            {
                MessageBox.Show("Select Row To delete.");
            }
        }

        private Evaluation inputEvaluation()
        {
            string name = E_Name.Text.ToString();
            if (name == "" || name.Length > 200 || !Evaluation.IsEvaluationNameUnique(name))
            {
                MessageBox.Show("Enter valid Name.");
                return null;
            }
            string marks = E_Marks.Text;
            string weightage = E_Weightage.Text;
            int e_marks = 0;
            int e_weightage = 0;
            if (Validator.ValidateInteger(marks))
            {
                e_marks = int.Parse(E_Marks.Text);
            }
            else
            {
                MessageBox.Show("Enter Valid Marks.");
                return null;
            }
            if (Validator.ValidateInteger(weightage))
            {
                e_weightage = int.Parse(E_Weightage.Text);
            }
            else
            {
                MessageBox.Show("Enter Valid Weightage.");
                return null;
            }
            Evaluation eval = new Evaluation(name, e_marks, e_weightage);
            return eval;
        }
        private void Add_Eval_Click(object sender, EventArgs e)
        {
            Evaluation eval = inputEvaluation();
            if (eval != null)
            {
                Evaluation.AddEvaluation(eval);
                loadEvaluations();
                clearEvalPage();
            }
        }

        private void clearEvalPage()
        {
            E_Marks.Text = "";
            E_Name.Text = "";
            E_Weightage.Text = "";
        }

        private void View_Eval_Click(object sender, EventArgs e)
        {
            loadEvaluations();
        }

        private void loadEvaluations()
        {
            DataTable dt = Evaluation.GetEvaluationDetails();
            dataGridView4.DataSource = dt;
            dataGridView4.Columns["Id"].Visible = false;
        }
        private void Search_Eval_Click(object sender, EventArgs e)
        {
            string searchTerm = E_Name.Text;
            if (E_Name.Text == "")
            {
                MessageBox.Show("Enter Evaluation title to search it.");
                return;
            }
            else
            {
                DataTable dt = Evaluation.SearchEvaluation(searchTerm);
                dataGridView4.DataSource = dt;
                dataGridView4.Columns["Id"].Visible = false;
            }
        }

        private void Delete_Eval_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView4.SelectedRows[0];
                int id = Convert.ToInt32(selectedRow.Cells["Id"].Value);

                bool flag = Evaluation.MarkEvaluationDeleted(id);
                if (flag)
                {
                    MessageBox.Show("Successfully deleted.");
                    loadEvaluations();
                }
            }
            else
            {
                MessageBox.Show("Select Row to delete");
            }
        }

        private void Upd_Eval_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView4.SelectedRows[0];
                int id = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                string Name = selectedRow.Cells["Name"].Value.ToString();
                String title = "Update Evaluation";
                string message = "Have you entered the updated values in the texboxes?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, title, buttons);
                if (result == DialogResult.Yes)
                {
                    string name = E_Name.Text.ToString();
                    if (name == "" || name.Length > 200 )
                    {
                        MessageBox.Show("Enter valid Name.");
                        return;
                    }
                    string marks = E_Marks.Text;
                    string weightage = E_Weightage.Text;
                    int e_marks = 0;
                    int e_weightage = 0;
                    if (Validator.ValidateInteger(marks))
                    {
                        e_marks = int.Parse(E_Marks.Text);
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Marks.");
                        return;
                    }
                    if (Validator.ValidateInteger(weightage))
                    {
                        e_weightage = int.Parse(E_Weightage.Text);
                    }
                    else
                    {
                        MessageBox.Show("Enter Valid Weightage.");
                        return;
                    }
                    Evaluation ev = new Evaluation(name, e_marks, e_weightage);
                    if (ev != null)
                    {
                        if (Evaluation.UpdateEvaluation(id, ev))
                        {
                            MessageBox.Show("Updated Successfully.");
                            clearEvalPage();
                            E_Name.Enabled = true;
                            loadEvaluations();
                        }
                    }
                }
                else
                {
                    E_Name.Text = Name;
                    E_Name.Enabled = false;
                }

            }
            else
            {
                MessageBox.Show("Select Row to update");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 4;
            tabControl1.Visible = true;
            p_Title.Enabled = true;

        }

        private void Evaluations_Crud_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 5;
            E_Name.Enabled = true;
            tabControl1.Visible = true;
        }

        private void panel14_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Add_Persons_Enter(object sender, EventArgs e)
        {
            Add_Persons.BackColor = Color.CadetBlue;
            Add_Persons.ForeColor = Color.White;

        }

        private void Add_Persons_Leave(object sender, EventArgs e)
        {
            Add_Persons.BackColor = Color.White;
            Add_Persons.ForeColor = Color.Teal;
        }



        private void LoadDataIntoStudentsComboBox()
        {
            Student_list.Items.Clear();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"SELECT s.RegistrationNo
                                FROM Student s
                                JOIN Person p ON p.Id = s.Id
                                LEFT JOIN Lookup l ON p.Gender = l.Id
                                WHERE p.FirstName NOT LIKE '*%'
                                AND s.RegistrationNo NOT IN (
                                    SELECT s.RegistrationNo
                                    FROM Student s
                                    JOIN GroupStudent gs ON s.Id = gs.StudentId
                                    WHERE gs.Status IS NOT NULL AND gs.Status <> 4 )";
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Student_list.Items.Add(reader["RegistrationNo"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading data: " + ex.Message);
            }
        }

        private void create_Group_Click(object sender, EventArgs e)
        {
            Group.CleanupInactiveGroups();
            string msg = Group.add_Group();
            MessageBox.Show(msg);
            MessageBox.Show("Select students and assign Project.");
            LoadDataIntoProjectsComboBox();
            LoadDataIntoStudentsComboBox();
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 7;
            tabControl1.Visible = true;
            Assign_Project.Visible = true;
            Add_Group_Stu.Visible = true;
            DelGroupStu.Visible = false;
            UpdateAssignedPro.Visible = false;
            label31.Visible = false;
            label32.Visible = false;
            label33.Visible = false;
            AddNewStu.Visible = false;
            SpecificGroupStu.DataSource = null;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 6;
            tabControl1.Visible = true;
        }

        private void LoadDataIntoProjectsComboBox()
        {
            projects_list.Items.Clear();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT P.Id, P.Title FROM Project P " +
                       "LEFT JOIN GroupProject GP ON P.Id = GP.ProjectId " +
                       "WHERE GP.ProjectId IS NULL And P.Title Not Like '*%'";
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            projects_list.Items.Add(reader["Title"]);
                            projects_list.Tag = reader["Id"];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading data: " + ex.Message);
            }
        }


        private void Assign_Project_Click(object sender, EventArgs e)
        {
            if (projects_list.SelectedItem != null)
            {
                string title = projects_list.SelectedItem.ToString();
                int gid = Group.getId();
                int pid = Project.GetProjectIdByTitle(title);
                if (pid != -1)
                {
                    DateTime assign = DateTime.Now;

                    bool success = GroupStudent.AddGroupProject(pid, gid, assign);

                    if (success)
                    {
                        MessageBox.Show("Project assigned to the group successfully.");
                        Group.CleanupInactiveGroups();
                        DataTable dt = GroupStudent.ViewGroupDetails();
                        groups_list.DataSource = dt;
                    }
                    else
                    {
                        MessageBox.Show("Failed to assign project to the group.");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid project ID.");
                }
            }
            else
            {
                MessageBox.Show("Please select a project to assign.");
            }
        }

        private void Add_Group_Stu_Click(object sender, EventArgs e)
        {
            int gid = Group.getId();
            add_STU_In_Group(gid);
            Group.CleanupInactiveGroups();
            DataTable dt = GroupStudent.ViewGroupDetails();
            groups_list.DataSource = dt;

        }


        private void add_STU_In_Group(int gid)
        {
            if (Student_list.SelectedIndex >= 0)
            {

                string registrationNumber = Student_list.SelectedItem.ToString();
                int sid = Student.GetStudentId(registrationNumber);
                DateTime assign = DateTime.Now;
                int status = 3;

                GroupStudent gs = new GroupStudent(gid, sid, status, assign);
                bool success = GroupStudent.AddGroupStudent(gs);

                if (success)
                {
                    MessageBox.Show("Student assigned to the group successfully.");
                    LoadDataIntoStudentsComboBox();
                }
                else
                {
                    MessageBox.Show("Failed to assign Student to the group.");
                }
            }
            else
            {
                MessageBox.Show("Please select a student to add.");
            }
        }
        private void View_Groups_Click(object sender, EventArgs e)
        {
            Group.CleanupInactiveGroups();
            DataTable dt = GroupStudent.ViewGroupDetails();
            groups_list.DataSource = dt;
        }


        private void LoadProjectsWithoutAdvisors()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                               SELECT Id, Title 
                        FROM Project 
                        WHERE Id NOT IN (
                            SELECT ProjectId 
                            FROM ProjectAdvisor 
                            WHERE AdvisorRole IN (11, 12, 14)
                        ) AND Title NOT LIKE '*%'";
                adv_projects.Items.Clear();
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            adv_projects.Items.Add(reader["Title"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading projects without advisors: " + ex.Message);
            }
        }


        private void LoadAllProjects()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                               SELECT Id, Title 
                        FROM Project 
                        WHERE Title NOT LIKE '*%'";
                adv_projects.Items.Clear();
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            adv_projects.Items.Add(reader["Title"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading projects without advisors: " + ex.Message);
            }
        }
        private void Assign_Advisor_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 8;
            tabControl1.Visible = true;
            LoadProjectsWithoutAdvisors();
            LoadAdvisorsForProject(-1, adv_list);
            LoadAdvisorsForProject(-1, co_adv_list);
            LoadAdvisorsForProject(-1, ind_adv_list);

        }

        private void LoadAdvisorsForProject(int projectId, ComboBox adv_list)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                SELECT a.Id as AdvisorId, CONCAT(p.FirstName, ' ', p.LastName) as Name
                FROM Advisor a
                INNER JOIN Person p ON a.Id = p.Id
                WHERE a.Id NOT IN (
                    SELECT AdvisorId 
                    FROM ProjectAdvisor 
                    WHERE ProjectId = @projectId
                )   And p.FirstName Not Like '*%'";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@projectId", projectId);
                    // Clear the ComboBox items before adding new ones
                    adv_list.Items.Clear();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int advisorId = (int)reader["AdvisorId"];
                            string advisorName = (string)reader["Name"];

                            // Create a KeyValuePair to store AdvisorId and advisorName
                            KeyValuePair<int, string> advisorItem = new KeyValuePair<int, string>(advisorId, advisorName);

                            adv_list.Items.Add(advisorItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading advisors for project: " + ex.Message);
            }
        }


        private void Add_adv_Click(object sender, EventArgs e)
        {
            if (adv_projects.SelectedItem != null && adv_list.SelectedItem != null)
            {
                string project = adv_projects.SelectedItem.ToString();
                int pid = Project.GetProjectIdByTitle(project);
                KeyValuePair<int, string> selectedAdvisor = (KeyValuePair<int, string>)adv_list.SelectedItem;
                int advisorId = selectedAdvisor.Key;

                // Check if an advisor of the specified role is already assigned to the project
                if (Project.IsAdvisorAssigned(pid, 11))
                {
                    string message = "Main Advisor is already assigned to this project. Do you want to update?";
                    string title = "Update Advisor";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result = MessageBox.Show(message, title, buttons);

                    if (result == DialogResult.Yes)
                    {
                        int oldAdvisorId = Project.GetAssignedAdvisorId(pid, 11);
                        if (Project.UpdateAssignedAdvisor(pid, oldAdvisorId, advisorId, 11)) 
                        {
                            MessageBox.Show("Main Advisor Updated Successfully.");
                        }
                        LoadAdvisorsForProject(pid, co_adv_list);
                        LoadAdvisorsForProject(pid, ind_adv_list);
                    }
                }
                else
                {
                    if (Project.AssignAdvisorToProject(pid, advisorId, 11))
                    {
                        MessageBox.Show("Main Advisor Assigned Successfully.");
                    }
                    LoadAdvisorsForProject(pid, co_adv_list);
                    LoadAdvisorsForProject(pid, ind_adv_list);
                }
            }
            else
            {
                MessageBox.Show("Select Project and Advisor First.");
            }
        }


        private void Add_co_adv_Click(object sender, EventArgs e)
        {
            if (adv_projects.SelectedItem != null && co_adv_list.SelectedItem != null)
            {
                string project = adv_projects.SelectedItem.ToString();
                int pid = Project.GetProjectIdByTitle(project);
                KeyValuePair<int, string> selectedAdvisor = (KeyValuePair<int, string>)co_adv_list.SelectedItem;
                int advisorId = selectedAdvisor.Key;

                // Check if an advisor of the specified role is already assigned to the project
                if (Project.IsAdvisorAssigned(pid, 12))
                {
                    string message = "Co Advisor is already assigned to this project. Do you want to update?";
                    string title = "Update Advisor";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result = MessageBox.Show(message, title, buttons);

                    if (result == DialogResult.Yes)
                    {
                        int oldAdvisorId = Project.GetAssignedAdvisorId(pid, 12);
                        if(Project.UpdateAssignedAdvisor(pid, oldAdvisorId, advisorId, 12))
                        {
                            MessageBox.Show("Updated co advisor successfully.");
                        }
                        LoadAdvisorsForProject(pid, adv_list);
                        LoadAdvisorsForProject(pid, ind_adv_list);
                    }
                }
                else
                {
                    if (Project.AssignAdvisorToProject(pid, advisorId, 12))
                    {
                        MessageBox.Show("Co Advisor Assigned Successfully.");
                    }
                    LoadAdvisorsForProject(pid, adv_list);
                    LoadAdvisorsForProject(pid, ind_adv_list);
                }
            }
            else
            {
                MessageBox.Show("Select Project and Advisor First.");
            }
        }

        private void Add_Ind_adv_Click(object sender, EventArgs e)
        {
            if (adv_projects.SelectedItem != null && ind_adv_list.SelectedItem != null)
            {
                string project = adv_projects.SelectedItem.ToString();
                int pid = Project.GetProjectIdByTitle(project);
                KeyValuePair<int, string> selectedAdvisor = (KeyValuePair<int, string>)ind_adv_list.SelectedItem;
                int advisorId = selectedAdvisor.Key;

                // Check if an advisor of the specified role is already assigned to the project
                if (Project.IsAdvisorAssigned(pid, 14))
                {
                    string message = "Industry Advisor is already assigned to this project. Do you want to update?";
                    string title = "Update Advisor";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result = MessageBox.Show(message, title, buttons);

                    if (result == DialogResult.Yes)
                    {
                        int oldAdvisorId = Project.GetAssignedAdvisorId(pid, 14);
                        if (Project.UpdateAssignedAdvisor(pid, oldAdvisorId, advisorId, 14))
                        {
                            MessageBox.Show("Updated Industry advisor successfully.");
                        }
                        LoadAdvisorsForProject(pid, adv_list);
                        LoadAdvisorsForProject(pid, co_adv_list);
                    }
                }
                else
                {
                    if (Project.AssignAdvisorToProject(pid, advisorId, 14))
                    {
                        MessageBox.Show("Industry Advisor Assigned Successfully.");
                    }
                    LoadAdvisorsForProject(pid, adv_list);
                    LoadAdvisorsForProject(pid, co_adv_list);
                }
            }
            else
            {
                MessageBox.Show("Select Project and Advisor First.");
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel14_Paint_1(object sender, PaintEventArgs e)
        {
            panel14.BackColor = Color.FromArgb(180, Color.White);
        }

        private void Delete_Group_Click(object sender, EventArgs e)
        {
            if (groups_list.SelectedRows != null && groups_list.SelectedRows.Count > 0)
            {
                int gid = (int)groups_list.SelectedRows[0].Cells["GroupId"].Value;
                bool flag = Group.DeleteGroup(gid);
                if(flag)
                {
                    MessageBox.Show("Deleted Successfully.");
                }
            }
            else
            {
                DataTable dt = GroupStudent.ViewGroupDetails();
                groups_list.DataSource = dt;
                MessageBox.Show("Select a group to delete.");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (groups_list.SelectedRows != null && groups_list.SelectedRows.Count > 0)
            {
                int gid = (int)groups_list.SelectedRows[0].Cells["GroupId"].Value;
                upd_group_id = gid;
                Group.LoadStudentsForGroup(gid, SpecificGroupStu);
                LoadDataIntoProjectsComboBox();
                LoadDataIntoStudentsComboBox();
                tabControl1.Visible = false;
                tabControl1.SelectedIndex = 7;
                tabControl1.Visible = true;
                Assign_Project.Visible = false;
                Add_Group_Stu.Visible = false;
                DelGroupStu.Visible = true;
                UpdateAssignedPro.Visible = true;
                label31.Visible = true;
                label32.Visible = true;
                label33.Visible = true;
                AddNewStu.Visible = true;
            }
            else
            {
                DataTable dt = GroupStudent.ViewGroupDetails();
                groups_list.DataSource = dt;
                MessageBox.Show("Select a group to update.");
            }
        }

        private void DelGroupStu_Click(object sender, EventArgs e)
        {
            if (SpecificGroupStu.SelectedRows != null && SpecificGroupStu.SelectedRows.Count > 0)
            {
                string reg = SpecificGroupStu.SelectedRows[0].Cells["RegistrationNo"].Value.ToString();
                int sid = Student.GetStudentId(reg);
                GroupStudent.RemoveStudentFromGroup(sid,upd_group_id);
                Group.CleanupInactiveGroups();
                DataTable dt = GroupStudent.ViewGroupDetails();
                MessageBox.Show("Student Deleted From the Group Successfully.");
                Group.LoadStudentsForGroup(upd_group_id, SpecificGroupStu);
                groups_list.DataSource = dt;
            }
            else
            {
                MessageBox.Show("Select a student to remove from group.");
            }
        }

        private void AddNewStu_Click(object sender, EventArgs e)
        {
            add_STU_In_Group(upd_group_id);
            Group.CleanupInactiveGroups();
            Group.LoadStudentsForGroup(upd_group_id, SpecificGroupStu);
            DataTable dt = GroupStudent.ViewGroupDetails();
            groups_list.DataSource = dt;
        }

        private void UpdateAssignedPro_Click(object sender, EventArgs e)
        {
            if (projects_list.SelectedItem != null)
            {
                string title = projects_list.SelectedItem.ToString();
                int pid = Project.GetProjectIdByTitle(title);
                if (pid != -1)
                {
                    DateTime assign = DateTime.Now;

                    bool success = GroupStudent.AddGroupProject(pid, upd_group_id, assign);

                    if (success)
                    {
                        MessageBox.Show("Project updated successfully.");
                        Group.CleanupInactiveGroups();
                        DataTable dt = GroupStudent.ViewGroupDetails();
                        Group.LoadStudentsForGroup(upd_group_id, SpecificGroupStu);
                        groups_list.DataSource = dt;
                    }
                    else
                    {
                        MessageBox.Show("Failed to update.");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid project ID.");
                }
            }
            else
            {
                MessageBox.Show("Please select a project to assign.");
            }
        }

        private void Load_Distinct_Pro_Click(object sender, EventArgs e)
        {
            LoadProjectsWithoutAdvisors();
        }

        private void Load_All_Projects_Click(object sender, EventArgs e)
        {
            LoadAllProjects();
        }

        private void label34_Click(object sender, EventArgs e)
        {

        }

        private void Conduct_Evaluations_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            Save_upd_evl.Visible = false;
            group_ids_list.Enabled = true;
            evaluations_list.Enabled = true;
            tabControl1.SelectedIndex = 9;
            tabControl1.Visible = true;
            Evaluation.LoadEvaluationsComboBox(evaluations_list);
            dataGridView5.DataSource = Evaluation.GetAllEvaluatedGroups();

        }

        private void Manage_Reports_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 10;
            tabControl1.Visible = true;
        }

        private void evaluations_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ev_Name = evaluations_list.SelectedItem.ToString();
            int id = Evaluation.GetEvaluationIdByTitle(ev_Name);
            int total = Evaluation.GetTotalMarksOfEvaluation(id);
            Total_marks_label.Text = total.ToString();
            var groupIds = Group.LoadGroupIdsNotEvaluated(id);

            // Clear existing items before adding new ones
            group_ids_list.Items.Clear();

            foreach (int groupId in groupIds)
            {
                group_ids_list.Items.Add("Pid-" + groupId);
            }
        }

        private void add_evaluate_group_Click(object sender, EventArgs e)
        {
            if(group_ids_list.SelectedItem == null || Obtained_Marks.Text == "")
            {
                MessageBox.Show("Select Group and Enter marks first.");
            }
            else
            {
                string group = group_ids_list.SelectedItem.ToString();
                var id = group.Split('-');
                int gid = int.Parse(id[1]);
                string ev_Name = evaluations_list.SelectedItem.ToString();
                int eid = Evaluation.GetEvaluationIdByTitle(ev_Name);
                string marks = Obtained_Marks.Text.ToString();
                if(!Validator.ValidateInteger(marks))
                {
                    MessageBox.Show("You can add only numbers in marks.");
                }
                else
                {
                    int obt_marks = int.Parse(marks);
                    if (obt_marks > int.Parse(Total_marks_label.Text.ToString()))
                    {
                        MessageBox.Show("Obtained marks cannot be greater than total marks.");
                    }
                    else
                    {
                       if (Evaluation.AddEvaluationForGroup(gid, eid, obt_marks))
                        {
                            MessageBox.Show(gid + " evaluated successfully");
                            dataGridView5.DataSource = Evaluation.GetAllEvaluatedGroups();
                        }
                    }
                }
            }
        }

        private void upd_evl_grp_Click(object sender, EventArgs e)
        {
            if(dataGridView5.SelectedRows.Count>0)
            {
                string gid = dataGridView5.SelectedRows[0].Cells["GroupId"].Value.ToString();
                string eid = dataGridView5.SelectedRows[0].Cells["EvaluationName"].Value.ToString();
                Save_upd_evl.Visible = true;
                group_ids_list.Text= gid;
                evaluations_list.Text = eid;
                group_ids_list.Enabled = false;
                evaluations_list.Enabled = false;
                
            }
            else
            {
                MessageBox.Show("Select the row to update.");
            }
        }

        private void Save_upd_evl_Click(object sender, EventArgs e)
        {
            try
            {
                string group = group_ids_list.Text.ToString();
                var id = group.Split('-');
                int gid = int.Parse(id[1]);
                string ev_Name = evaluations_list.Text.ToString();
                int eid = Evaluation.GetEvaluationIdByTitle(ev_Name);
                string marks = Obtained_Marks.Text.ToString();
                if (!Validator.ValidateInteger(marks))
                {
                    MessageBox.Show("You can add only numbers in marks.");
                }
                else
                {
                    int obt_marks = int.Parse(marks);
                    if (obt_marks > int.Parse(Total_marks_label.Text.ToString()))
                    {
                        MessageBox.Show("Obtained marks cannot be greater than total marks.");
                    }
                    else
                    {
                        if (Evaluation.UpdateObtainedMarksForGroup(gid, eid, obt_marks))
                        {
                            MessageBox.Show(gid + " evaluated  updated successfully");
                            dataGridView5.DataSource = Evaluation.GetAllEvaluatedGroups();
                            Save_upd_evl.Visible = false;
                            group_ids_list.Enabled = true;
                            evaluations_list.Enabled = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void advisory_board_report_Click(object sender, EventArgs e)
        {
            Pdf.CustomizeAdvisoryReport();
        }

        private void Generate_Mark_Sheet_Click(object sender, EventArgs e)
        {
            Pdf.MarkSheet();
        }

        private void top10groups_Click(object sender, EventArgs e)
        {
            Pdf.GenerateTopGroupsReport();
        }

        private void student_who_changed_group_Click(object sender, EventArgs e)
        {
            Pdf.GenerateStudentGroupChangeReport();
        }

        private void ProjectAllocationReport_Click(object sender, EventArgs e)
        {
            Pdf.GenerateProjectAllocationOverviewReport();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Back_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = false;
            tabControl1.SelectedIndex = 6;
            tabControl1.Visible = true;
        }
    }
}
