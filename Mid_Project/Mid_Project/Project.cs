﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public class Project
    {
        public int ProjectId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Project(string title, string description)
        {
            this.Title = title;
            this.Description = description;
        }
        public static string AddProject(Project project)
        {
            var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "INSERT INTO Project (Title, Description) VALUES (@Title, @Description)";
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@Title", project.Title);
                command.Parameters.AddWithValue("@Description", project.Description);
                command.ExecuteNonQuery();
                return "Project added successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static DataTable GetAllProjects()
        {
            DataTable dt = new DataTable();
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Id, Title, Description FROM Project WHERE Title NOT LIKE '*%' ";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                // Allow DBNull for the Description column
                dt.Columns["Description"].AllowDBNull = true;

                // Replace empty strings with null for Description column
                foreach (DataRow row in dt.Rows)
                {
                    if (row["Description"] != DBNull.Value && string.IsNullOrWhiteSpace(row["Description"].ToString()))
                    {
                        row["Description"] = DBNull.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show(ex.Message);
                MessageBox.Show("Failed to retrieve projects. Please try again.");
            }
            return dt;
        }



        public static string UpdateProject(int projectId, Project project)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "UPDATE Project SET Title = @Title, Description = @Description WHERE Id = @ProjectId";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@Title", project.Title);
                cmd.Parameters.AddWithValue("@Description", project.Description);
                cmd.Parameters.AddWithValue("@ProjectId", projectId);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    return "Project updated successfully.";
                }
                else
                {
                    return "Failed to update project.";
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public static string DeleteProject(int projectId)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Project SET Title = '*' + Title WHERE Id = @ProjectId";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@ProjectId", projectId);
                    int rowsAffected = cmd.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        return "Project deleted successfully.";
                    }
                    else
                    {
                        return "Retry to delete project.";
                    }
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public static Project GetProjectById(int projectId)
        {
            Project project = null;
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT Title, Description FROM Project WHERE Id = @ProjectId";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@ProjectId", projectId);
                SqlDataReader reader = cmd.ExecuteReader();

                // Check if there are rows returned
                if (reader.Read())
                {
                    // Extract project details from the reader
                    string title = reader["Title"].ToString();
                    string description = reader["Description"].ToString();

                    // Create a new Project object
                    project = new Project(title, description);
                }

                // Close the reader and connection
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show(ex.Message);
            }

            return project;
        }

        public static bool IsTitleUnique(string title)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT COUNT(*) FROM Project WHERE Title = @Title";
                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@Title", title);

                    int count = (int)command.ExecuteScalar();

                    // If count is 0, the title is unique
                    return count == 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error checking project title uniqueness: " + ex.Message);
                return false; 
            }
        }
        public static List<Project> SearchProjectByTitle(string title)
        {
            List<Project> projects = new List<Project>();

            try
            {
                var con = Configuration.getInstance().getConnection();
                {
                    // SQL query to search for projects with titles similar to the provided title
                    string query = "SELECT Id, Title, Description FROM Project WHERE Title LIKE @Title And Title Not Like '*%'";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@Title", "%" + title + "%");
                    
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int projectId = Convert.ToInt32(reader["Id"]);
                            string projectTitle = reader["Title"].ToString();
                            string description = reader["Description"].ToString();

                            Project project = new Project(projectTitle, description);
                            projects.Add(project);
                        }
                    }
                    

                }
                return projects;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show(ex.Message);
                return null;
            }

            
        }

        public static bool AssignAdvisorToProject(int projectId, int advisorId, int advisor_Role)
        {
            try
            {
                    SqlConnection con = Configuration.getInstance().getConnection();
                    string query = "INSERT INTO ProjectAdvisor (ProjectId, AdvisorId, AdvisorRole, AssignmentDate) " +
                                   "VALUES (@ProjectId, @AdvisorId, @Advisor_Role, @AssignmentDate)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@ProjectId", projectId);
                    cmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                    cmd.Parameters.AddWithValue("@Advisor_Role", advisor_Role);
                    cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);

                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error assigning advisor to project: " + ex.Message);
                return false;
            }
        }
        public static int GetProjectIdByTitle(string projectTitle)
        {
            int projectId = -1; // Default value if project ID is not found

            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT Id FROM Project WHERE Title = @Title";
                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@Title", projectTitle);

                    object result = command.ExecuteScalar();
                    if (result != null && result != DBNull.Value)
                    {
                        projectId = Convert.ToInt32(result);
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error getting project ID by title: " + ex.Message);
            }

            return projectId;
        }


        public static bool IsAdvisorAssigned(int projectId, int advisorRole)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                SELECT COUNT(*)
                FROM ProjectAdvisor
                WHERE ProjectId = @projectId
                AND AdvisorId IS NOT NULL
                AND AdvisorRole = @advisorRole";

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@projectId", projectId);
                        command.Parameters.AddWithValue("@advisorRole", advisorRole);

                        int count = (int)command.ExecuteScalar();

                        // If count > 0, it means an advisor of the specified role is already assigned to the project
                        return count > 0;
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }

        public static bool UpdateAssignedAdvisor(int projectId, int oldAdvisorId, int newAdvisorId, int advisorRole)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                UPDATE ProjectAdvisor
                SET AdvisorId = @newAdvisorId
                WHERE ProjectId = @projectId
                AND AdvisorId = @oldAdvisorId
                AND AdvisorRole = @advisorRole";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                        command.Parameters.AddWithValue("@projectId", projectId);
                        command.Parameters.AddWithValue("@oldAdvisorId", oldAdvisorId);
                        command.Parameters.AddWithValue("@newAdvisorId", newAdvisorId);
                        command.Parameters.AddWithValue("@advisorRole", advisorRole);

                        int rowsAffected = command.ExecuteNonQuery();

                        // If rowsAffected > 0, it means the update was successful
                        return rowsAffected > 0;
                 }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error updating assigned advisor: " + ex.Message);
                return false;
            }
        }

        public static int GetAssignedAdvisorId(int projectId, int advisorRole)
        {
            int advisorId = -1; // Default value if no advisor is found

            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = @"
                                    SELECT AdvisorId
                                    FROM ProjectAdvisor
                                    WHERE ProjectId = @projectId
                                    AND AdvisorRole = @advisorRole";

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@projectId", projectId);
                        command.Parameters.AddWithValue("@advisorRole", advisorRole);

                        // Execute the query and get the advisor ID
                        object result = command.ExecuteScalar();

                        // Check if the result is not null
                        if (result != null)
                        {
                            advisorId = Convert.ToInt32(result);
                        }
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error getting assigned advisor ID: " + ex.Message);
            }

            return advisorId;
        }


    }
}
