﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public class Evaluation
    {
        public int Marks { get; set; }
        public string Name { get; set; }
        public int Weightage { get; set; }

        public Evaluation(string Name, int Marks, int Weightage)
        {
            this.Marks = Marks;
            this.Name = Name;
            this.Weightage = Weightage;
        }
        public static bool AddEvaluation(Evaluation e)
        {
            try
            {
                    SqlConnection con = Configuration.getInstance().getConnection();
                    string query = "INSERT INTO Evaluation (Name, TotalMarks, TotalWeightage) " +
                                   "VALUES (@Name, @TotalMarks, @TotalWeightage)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@Name", e.Name);
                    cmd.Parameters.AddWithValue("@TotalMarks", e.Marks);
                    cmd.Parameters.AddWithValue("@TotalWeightage", e.Weightage);

                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }


        public static DataTable GetEvaluationDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT * FROM Evaluation Where Name Not Like '*%'";
                    SqlCommand cmd = new SqlCommand(query, con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;
        }

        public static bool UpdateEvaluation(int id, Evaluation e)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Evaluation SET Name = @Name, TotalMarks = @TotalMarks, TotalWeightage = @TotalWeightage " +
                                   "WHERE Id = @Id";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Parameters.AddWithValue("@Name", e.Name);
                    cmd.Parameters.AddWithValue("@TotalMarks", e.Marks);
                    cmd.Parameters.AddWithValue("@TotalWeightage", e.Weightage);
                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }

        public static bool MarkEvaluationDeleted(int id)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Evaluation SET Name = '*' + Name WHERE Id = @Id";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@Id", id);

                    int rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }



        public static DataTable SearchEvaluation(string searchTerm)
        {
            DataTable dt = new DataTable();
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Evaluation WHERE Name LIKE @SearchTerm   And Name Not Like '*%'";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@SearchTerm", "%" + searchTerm + "%");

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dt;
        }


        public static bool IsEvaluationNameUnique(string evaluationName)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT COUNT(*) FROM Evaluation WHERE Name = @Name";
                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@Name", evaluationName);

                    int count = (int)command.ExecuteScalar();

                    return count == 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error checking evaluation name uniqueness: " + ex.Message);
                return false; 
            }
        }

        public static void LoadEvaluationsComboBox(ComboBox evaluations_combo)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                                SELECT Name 
                                FROM Evaluation 
                                WHERE Name Not Like '*%'";  

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string title = (string)reader["Name"];
                            evaluations_combo.Items.Add(title);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading evaluations: " + ex.Message);
            }
        }

        public static int GetEvaluationIdByTitle(string title)
        {
            int evaluationId = -1; // Default value if not found

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
            SELECT Id
            FROM Evaluation
            WHERE Name = @title";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@title", title);

                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        evaluationId = Convert.ToInt32(result);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error getting evaluation ID: " + ex.Message);
            }

            return evaluationId;
        }
        public static int GetTotalMarksOfEvaluation(int evaluationId)
        {
            int totalMarks = 0;
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                SELECT TotalMarks
                FROM Evaluation
                WHERE Id = @evaluationId";

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@evaluationId", evaluationId);
                        object result = command.ExecuteScalar();

                        if (result != null && result != DBNull.Value)
                        {
                            totalMarks = Convert.ToInt32(result);
                        }
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving total marks of evaluation: " + ex.Message);
            }

            return totalMarks;
        }

        public static bool AddEvaluationForGroup(int groupId, int evaluationId, int obtainedMarks)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "INSERT INTO GroupEvaluation (GroupId, EvaluationId, ObtainedMarks, EvaluationDate) VALUES (@GroupId, @EvaluationId, @ObtainedMarks, @EvaluationDate)";
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        command.Parameters.AddWithValue("@GroupId", groupId);
                        command.Parameters.AddWithValue("@EvaluationId", evaluationId);
                        command.Parameters.AddWithValue("@ObtainedMarks", obtainedMarks);
                        command.Parameters.AddWithValue("@EvaluationDate", DateTime.Now);

                        int rowsAffected = command.ExecuteNonQuery();

                        return rowsAffected > 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error adding evaluation for group: " + ex.Message);
                return false;
            }
        }

        public static DataTable GetAllEvaluatedGroups()
        {
            DataTable dt = new DataTable();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                                SELECT 
                                    'pid-' + CONVERT(NVARCHAR, g.Id) AS GroupId,
                                    pro.Title AS AssignedProject,
                                    ev.Name AS EvaluationName,
                                    ge.ObtainedMarks,
                                    ev.TotalMarks
                                FROM 
                                    GroupEvaluation ge
                                    INNER JOIN [Group] g ON ge.GroupId = g.Id
                                    LEFT JOIN GroupProject gp ON g.Id = gp.GroupId
                                    LEFT JOIN Project pro ON gp.ProjectId = pro.Id
                                    LEFT JOIN Evaluation ev ON ge.EvaluationId = ev.Id";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error retrieving evaluated groups: " + ex.Message);
            }

            return dt;
        }

        public static bool UpdateObtainedMarksForGroup(int groupId, int evaluationId, int newObtainedMarks)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = "UPDATE GroupEvaluation SET ObtainedMarks = @NewObtainedMarks WHERE GroupId = @GroupId AND EvaluationId = @EvaluationId";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@GroupId", groupId);
                    command.Parameters.AddWithValue("@EvaluationId", evaluationId);
                    command.Parameters.AddWithValue("@NewObtainedMarks", newObtainedMarks);

                    int rowsAffected = command.ExecuteNonQuery();

                    return rowsAffected > 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error updating obtained marks for group: " + ex.Message);
                return false;
            }
        }

    }
}
