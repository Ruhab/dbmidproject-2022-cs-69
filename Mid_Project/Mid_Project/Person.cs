﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mid_Project
{
    public class Person
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Gender { get; set; }

        public Person(string firstName, string lastName, string contact, string email, DateTime dateOfBirth, int gender)
        {
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
            DateOfBirth = dateOfBirth;
            Gender = gender;
        }
    }
}
