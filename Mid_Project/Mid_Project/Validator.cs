﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mid_Project
{
    public class Validator
    {

            public static bool email(string email)
            {
                if (email.Length >= 30)
                {
                    return false;
                }
                string regex = @"^[^@\s]+@[^@\s]+\.(com|net|org|gov)$";
                return Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
            }
            public static bool name(string name)
            {
                return name.Length >= 3 && name.Length < 100;
            }
            public static bool ValidatePhoneNumber(string phoneNumber)
            {
                string regexPattern = @"^[0-9]{11}$";
                return Regex.IsMatch(phoneNumber, regexPattern) && !phoneNumber.StartsWith("-");
            }
            public static bool valid_name(string name)
            {
                return name.Length >= 3 && name.Length < 100;
            }
            public static bool ValidateRegistrationNumber(string registrationNumber)
            {
                string regexPattern = @"^\d{4}-[A-Z]+-\d+$";
                return Regex.IsMatch(registrationNumber, regexPattern);
            }
            public static bool isValidTitle(string title)
            {
            return title.Length <= 50;
            }
            public static string ValidateSalary(string salaryString)
            {
                decimal salary;
                if (decimal.TryParse(salaryString, out salary))
                {
                    // Check if the salary is non-negative
                    if (salary < 0)
                    {
                        return("Salary cannot be negative.");
                    }
                    // Additional validation rules can be added here
                    return null; // Validation passed
                }
                else
                {
                    return("Invalid salary format.");
                }
            }
            public static bool ValidateInteger(string input)
            {
                int result;
                return int.TryParse(input, out result);
            }

            public static  bool ValidateDOB(DateTime dob)
            {
                // Minimum and maximum allowed DOB
                DateTime minDOB = new DateTime(1970, 1, 1);
                DateTime maxDOB = DateTime.Now.AddYears(-18); // At least 18 years ago (2004 or earlier)

                // Check if the provided DOB falls within the acceptable range
                if (dob < minDOB || dob > maxDOB)
                {
                    return false;
                }

                return true;
            }


    }

}
