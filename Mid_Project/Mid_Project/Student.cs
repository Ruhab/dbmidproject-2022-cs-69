﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    class Student: Person
    {
        public string RegistrationNumber { get; set; }
        public Student(string firstName, string lastName, string contact, string email, DateTime dateOfBirth, int gender, string registrationNumber)
        : base(firstName, lastName, contact, email, dateOfBirth, gender)
        {
            RegistrationNumber = registrationNumber;
        }

        public static string AddStudent(Student stu)
        {
         var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) " +
               "VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender);" +
               "INSERT INTO Student (Id, RegistrationNo) VALUES (SCOPE_IDENTITY(), @RegistrationNO);";
                SqlCommand command = new SqlCommand(query,con);
                command.Parameters.AddWithValue("@FirstName", stu.FirstName);
                command.Parameters.AddWithValue("@LastName", stu.LastName);
                command.Parameters.AddWithValue("@Contact", stu.Contact);
                command.Parameters.AddWithValue("@Email", stu.Email);
                command.Parameters.AddWithValue("@DateOfBirth", stu.DateOfBirth);
                command.Parameters.AddWithValue("@Gender", stu.Gender);
                command.Parameters.AddWithValue("@RegistrationNO", stu.RegistrationNumber);
                command.ExecuteNonQuery();
                return "";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public static bool RegistrationNumberExists(string registrationNumber)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
               
                    string query = "SELECT COUNT(*) FROM Student WHERE RegistrationNo = @RegistrationNo";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                    int count = (int)command.ExecuteScalar();

                    return count > 0; // Return true if count is greater than 0, indicating the registration number exists
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

                // Assume the registration number exists in case of an error
                return false;
            }
        }

        public static string UpdateStudent(Student stu, int id)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Person " +
                       "SET FirstName = @FirstName, LastName = @LastName, Contact = @Contact, " +
                       "Email = @Email, DateOfBirth = @DateOfBirth, Gender = @Gender " +
                       "WHERE Id = @Id";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@FirstName", stu.FirstName);
                    command.Parameters.AddWithValue("@LastName", stu.LastName);
                    command.Parameters.AddWithValue("@Contact", stu.Contact);
                    command.Parameters.AddWithValue("@Email", stu.Email);
                    command.Parameters.AddWithValue("@DateOfBirth", stu.DateOfBirth);
                    command.Parameters.AddWithValue("@Gender", stu.Gender);
                    command.Parameters.AddWithValue("@RegistrationNo", stu.RegistrationNumber);
                    command.Parameters.AddWithValue("@Id", id);


                    int rowsAffected = command.ExecuteNonQuery();

                    return null;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

                return ex.Message; // Update failed
            }

        }


        public static bool DeleteStudentRegistration(string registrationNumber)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "DELETE FROM Student WHERE RegistrationNo = @RegistrationNo";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                    int rowsAffected = command.ExecuteNonQuery();

                    return rowsAffected > 0; // Return true if one or more rows were affected (deleted)
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);

                return false; // Deletion failed
            }
        }

        public static bool MarkStudentDeleted(string registrationNumber)
        {
            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "UPDATE Person SET FirstName = '*' + FirstName " +
                                   "FROM Student " +
                                   "INNER JOIN Person ON Student.Id = Person.Id " +
                                   "WHERE Student.RegistrationNo = @RegistrationNo";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                    int rowsAffected = command.ExecuteNonQuery();

                    return rowsAffected > 0; // Return true if one or more rows were affected (updated)
               
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return false; // Update failed
            }
        }


        public DataTable SearchStudentDataByRegistrationNumber(string registrationNumber)
        {
            DataTable dt = new DataTable();

            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT Student.RegistrationNo, Person.FirstName, Person.LastName, " +
                                   "Person.Contact, Person.Email, Person.DateOfBirth, Person.Gender " +
                                   "FROM Student " +
                                   "INNER JOIN Person ON Student.Id = Person.Id " +
                                   "WHERE Student.RegistrationNo = @RegistrationNo";

                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                MessageBox.Show("Failed to search for student data. Please try again.");
            }

            return dt;
        }
        public static Student GetStudentByRegistrationNumber(string registrationNumber)
        {
            Student student = null;
            var con = Configuration.getInstance().getConnection();
            try
            {
                string query = "SELECT Student.Id, Student.RegistrationNo, Person.FirstName, " +
                               "Person.LastName, Person.Contact, Person.Email, " +
                               "Person.DateOfBirth, Person.Gender " +
                               "FROM Student " +
                               "INNER JOIN Person ON Student.Id = Person.Id " +
                               "WHERE Student.RegistrationNo = @RegistrationNo";

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    
                    string RegistrationNo = reader["RegistrationNo"].ToString();
                    string FirstName = reader["FirstName"].ToString();
                    string LastName = reader["LastName"].ToString();
                    string Contact = reader["Contact"].ToString();
                    string Email = reader["Email"].ToString();
                    DateTime DateOfBirth = Convert.ToDateTime(reader["DateOfBirth"]);
                    int Gender = Convert.ToInt32(reader["Gender"]);
                    student = new Student(FirstName,LastName,Contact,Email,DateOfBirth,Gender,RegistrationNo);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return student;
        }

        public static int GetStudentId(string registrationNumber)
        {
            int studentId = -1; 

            try
            {
                    var con = Configuration.getInstance().getConnection();
                    string query = "SELECT Id FROM Student WHERE RegistrationNO = @RegistrationNumber";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", registrationNumber);

                    object result = cmd.ExecuteScalar();

                    // Check if the result is not null and is convertible to int
                    if (result != null && int.TryParse(result.ToString(), out int id))
                    {
                        studentId = id;
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return studentId;
        }

    }
}

