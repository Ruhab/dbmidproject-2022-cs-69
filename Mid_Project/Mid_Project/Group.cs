﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public class Group
    {
        public static int id;
        public static int getId()
        {
            return id;
        }
        public static string add_Group()
        {
            try
            {
                id = 0;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO [Group](Created_On) VALUES(@date); SELECT Id FROM [Group] ORDER BY Id Desc", con);
                cmd.Parameters.AddWithValue("@date", DateTime.Today);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    id = int.Parse(reader["Id"].ToString());
                }
                reader.Close();
                return("Group Pid-" + id + " Created Successfully");
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static void LoadStudentsForGroup(int groupId, DataGridView dataGridView)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                SELECT s.RegistrationNo, p.FirstName, p.LastName, p.Contact, p.Email
                FROM Student s
                JOIN Person p ON s.Id = p.Id
                JOIN GroupStudent gs ON s.Id = gs.StudentId
                WHERE gs.GroupId = @GroupId And gs.Status = 3 And p.FirstName Not Like '*%'";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@GroupId", groupId);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    dataGridView.DataSource = dataTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading students for the group: " + ex.Message);
            }
        }


        public static void CleanupInactiveGroups()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();

                // Delete from GroupProject
                string deleteGroupProjectQuery = @"
                                                DELETE FROM [GroupProject]
                                                WHERE ProjectId IN (
                                                    SELECT Id
                                                    FROM [Group]
                                                    WHERE Id NOT IN (
                                                        SELECT GroupId
                                                        FROM GroupStudent
                                                        GROUP BY GroupId
                                                    )
                                                )";

                using (SqlCommand command1 = new SqlCommand(deleteGroupProjectQuery, con))
                {
                    command1.ExecuteNonQuery();
                }

                // Delete from Group
                string deleteGroupQuery = @"
                                        DELETE FROM [Group]
                                        WHERE Id NOT IN (
                                            SELECT GroupId
                                            FROM GroupStudent
                                            GROUP BY GroupId
                                        )";

                using (SqlCommand command2 = new SqlCommand(deleteGroupQuery, con))
                {
                    int rowsAffected = command2.ExecuteNonQuery();
                    Console.WriteLine($"{rowsAffected} inactive groups deleted.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error cleaning up inactive groups: " + ex.Message);
            }
        }


        public static bool DeleteGroup(int groupId)
        {
            try
            {
                SqlConnection con = Configuration.getInstance().getConnection();
                string updateGroupStudentQuery = "UPDATE GroupStudent SET GroupId = '*' + CONVERT(NVARCHAR, GroupId) WHERE GroupId = @GroupId";
                SqlCommand updateGroupStudentCmd = new SqlCommand(updateGroupStudentQuery, con);
                updateGroupStudentCmd.Parameters.AddWithValue("@GroupId", groupId);
                updateGroupStudentCmd.ExecuteNonQuery();

                string updateGroupQuery = "UPDATE [Group] SET Id = '*' + CAST(@GroupId AS NVARCHAR) WHERE Id = @GroupId";
                SqlCommand updateGroupCmd = new SqlCommand(updateGroupQuery, con);
                updateGroupCmd.Parameters.AddWithValue("@GroupId", groupId);
                updateGroupCmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
        }



        public static List<int> LoadGroupIdsNotEvaluated(int evaluationId)
        {
            List<int> groupIds = new List<int>();

            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = @"
                                SELECT g.Id
                                FROM [Group] g
                                INNER JOIN GroupProject gp ON g.Id = gp.GroupId
                                WHERE g.Id NOT IN (
                                    SELECT eg.GroupId
                                    FROM GroupEvaluation eg
                                    WHERE eg.EvaluationId = @evaluationId
                                )";

                using (SqlCommand command = new SqlCommand(query, con))
                {
                    command.Parameters.AddWithValue("@evaluationId", evaluationId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int groupId = (int)reader["Id"];
                            groupIds.Add(groupId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading group IDs: " + ex.Message);
            }

            return groupIds;
        }

    }
}
